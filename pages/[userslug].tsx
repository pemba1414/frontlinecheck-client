import React from 'react'
import { useRouter } from 'next/router'
import PublicNews from './../components/public-news'

type QueryProps = {
    userslug: string
}

const UserPublicPage = (): React.ReactElement => {
    const router = useRouter()
    const query: QueryProps = router.query as QueryProps
  
    return (
        <>
            {query.userslug && <PublicNews userSlug={query.userslug} />}
        </>
    )
}

export default UserPublicPage
