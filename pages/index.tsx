import React from 'react'
import PublicNews from './../components/public-news'

const Landing = (): React.ReactElement => {
    return (
        <PublicNews />
    )
}

export default Landing