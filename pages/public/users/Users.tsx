import { Label, Input, Button, Form, Row, Col, FormGroup } from 'reactstrap'
import React, { useState } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faUnlock } from '@fortawesome/free-solid-svg-icons'
import UsersList from './UsersList';


// type details = {
//     email: string,
//     urlSlug: string,
//     createdDate: string,
// }

const Users = () => {
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [usersList, setUsersList] = useState<string>('');
    const [usersLength, setUsersLength] = useState<number>()
    const [totalPages, setTotalPages] = useState<number>()
    const [currentPage, setCurrentPage] = useState<number>(0)
    const [totalUsers, setTotalUsers] = useState<number>()
    const [authenticationFailed, setAuthenticationFailed] = useState(false)

    const formSubmit = (e) => {
        e.preventDefault();
        if (password === 'sNr@publ1C' && username === 'snrpublic') {
            axios.post(`/api/public/v2/users?page=0&sortBy=email&direction=ASC`, {
                password: "sNr@publ1C",
                username: "snrpublic"
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    setTotalUsers(response.data.totalElements)
                    setUsersList(response.data.list)
                    setTotalPages(response.data.totalPages)
                    setUsersLength(response.data.list.length)
                })
                .catch((error) => console.error(error))
        } else {
            setAuthenticationFailed(true);
        }
    }


    const handleChange = (currentPage) => {
        axios.post(`/api/public/v2/users?page=${currentPage}&sortBy=email&direction=ASC`, {
            password: "sNr@publ1C",
            username: "snrpublic"
        }, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                setTotalUsers(response.data.totalElements)
                setUsersList(response.data.list)
                setTotalPages(response.data.totalPages)
                setUsersLength(response.data.list.length)
            })
            .catch((error) => console.error(error))
    }

    const Submit = (users) => {
        axios.post(`/api/public/v2/users?page=${currentPage}&sortBy=email&direction=ASC`, {
            password: "sNr@publ1C",
            text: users,
            username: "snrpublic",
        },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                console.log(response.data.list)
                setUsersList(response.data.list)
                setTotalPages(response.data.totalPages)
                setUsersLength(response.data.list.length)
            })
            .catch((error) => console.error(error))
    }

    return (
        <div className="jumbotron result-warp public-page">
            <div className="container">
                <div>
                    {
                        usersList ?
                            <>
                                <UsersList totalUsers={totalUsers} formsubmit={Submit} callback={handleChange}
                                    userDetail={usersList} totalPages={totalPages} usersLength={usersLength} />
                            </> :
                            (authenticationFailed) ?
                                <div>
                                    <h4>Wrong username or password.</h4>
                                    <br />
                                    <a href="/public/users" className="btn btn-primary">Retry</a>
                                </div>
                                :
                                <>
                                   
                                    <div className="authenticate">
                                    <h3>Authentication for public page access</h3>
                                    <form onSubmit={formSubmit}>
                                        <Row>
                                            <Col md={4}>
                                                <div className="wrap-input100  m-b-16">
                                                    <Input type="text" className="input100 input-white" placeholder="Username" onChange={(e) => setUsername(e.target.value)} />
                                                    <span className="focus-input100"></span>
                                                    <span className="symbol-input100">
                                                        <span className="lnr lnr-envelope">
                                                            <FontAwesomeIcon icon={faEnvelope} />
                                                        </span>
                                                    </span>
                                                </div>
                                            </Col>
                                            <Col md={4}>
                                                <div className="wrap-input100  m-b-16">
                                                    <Input type="password" className="input100 input-white" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                                                    <span className="focus-input100"></span>
                                                    <span className="symbol-input100">
                                                        <span className="lnr lnr-unlock">
                                                            <FontAwesomeIcon icon={faUnlock} />
                                                        </span>
                                                    </span>
                                                </div>
                                            </Col>
                                            <Col md={2}>
                                                <button type="submit" className="btn btn-primary form-control">List Users </button>
                                            </Col>
                                        </Row>
                                    </form>
                                    </div>
                                </>
                    }
                </div>
            </div>
        </div>
    )
}

export default Users;