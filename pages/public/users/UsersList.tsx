import { Form, Input, Button, Row, Col } from 'reactstrap'
import { useState } from 'react'
import ReactPaginate from 'react-paginate'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

const UsersList = ({ userDetail, totalPages, usersLength, callback, formsubmit, totalUsers }) => {

    const [users, setUsers] = useState<string>('')

    const onChangePage = (data): void => {
        callback(data.selected)
    }

    const formSubmit = (e) => {
        e.preventDefault()
        formsubmit(users)
    }

    return (
        <div className="tabular-details">
            <h3>Total Number of Users: <span className="total"> {totalUsers}</span></h3>
            <h4>Users with Public Pages</h4>
            <Form onSubmit={formSubmit}>
                <Row>
                    <Col md={6}>
                        <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Name or Email" onChange={(e) => setUsers(e.target.value)} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-user">
                                    <FontAwesomeIcon icon={faUser} />
                                </span>
                            </span>
                        </div>
                    </Col>
                    <Col md={4}>
                        <Button type="submit" className="btn btn-primary">Search</Button>
                    </Col>
                </Row>
            </Form>

            <table className="m-t-20">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Created Date</th>
                    </tr>
                </thead>
                <tbody>
                    {userDetail.map((data, index) => {
                        return (
                            <tr key={index}>
                                <td>{data.email}</td>
                                <td><li><a href={`${process.env.NEXT_PUBLIC_API_HOST}${data.urlSlug}`} target="_blank">{data.urlSlug}</a></li></td>
                                <td>{data.createdDate}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            {
                usersLength <= 10 ?
                    <ReactPaginate
                        previousLabel={'PREV'}
                        nextLabel={'NEXT'}
                        breakLabel={'...'}
                        pageCount={totalPages}
                        onPageChange={onChangePage}
                        marginPagesDisplayed={1}
                        pageRangeDisplayed={1}
                        containerClassName={'pagination'}
                        subContainerClassName={'pagination-inner'}
                        activeLinkClassName={'pagination-active'}
                    /> :
                    <ReactPaginate
                        previousLabel={'PREV'}
                        nextLabel={'NEXT'}
                        breakLabel={'...'}
                        pageCount={totalPages}
                        onPageChange={onChangePage}
                        marginPagesDisplayed={3}
                        pageRangeDisplayed={3}
                        containerClassName={'pagination'}
                        subContainerClassName={'pagination-inner'}
                        activeLinkClassName={'pagination-active'}
                    />
            }

        </div>
    )
}

export default UsersList;