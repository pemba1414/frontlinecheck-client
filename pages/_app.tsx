import App from 'next/app'
import { wrapper } from '../redux/store';
import { withRouter } from 'next/router'
import AdminLayout from '../layouts/admin-layout';
import DefaultLayout from "../layouts/default-layout";
import * as React from "react";
import Head from 'next/head';

import '../styles/globals.css'
import '../styles/component.css'
import '../styles/demo.css'
import '../styles/landingpage.css'
import '../styles/main.css'
import '../styles/website.css'
import '../node_modules/bootstrap/scss/bootstrap.scss'
import '../styles/update.css'
import '../styles/util.css'
import initialize from "../utils/initialize";
import Router from 'next/router'
import { createGzip } from 'node:zlib';


class MyApp extends App {

    componentDidMount() {
        Router.events.on('routeChangeComplete', () => {
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        });
    }

    static getInitialProps = async ({ Component, ctx }) => {
        let meta = {
            title: 'Frontlinecheck',
            description: 'अब तपाईंलाई चहिएको सबै समाचार एकै ठाउँम पाउनुहोस्',
            imagePath: '/images/600x315-frontline-01.png'
        }

        if (ctx.query !== undefined) {
            let path = `${process.env.NEXT_PUBLIC_API_HOST}api/public/v2/meta`
            const userUri = ctx.query.userslug
            if (userUri !== undefined && userUri !== '') {
                path += `?userUri=${userUri}`

                const newsUri = ctx.query.uri
                path += `&newsUri=${newsUri !== undefined ? encodeURIComponent(newsUri) : ''}`

                const res = await fetch(path)
                let json = await res.json()
                meta.title = json.title !== null ? json.title : meta.title
                meta.description = (json.description !== null && json.description !== undefined) ? json.description.replace(/(<([^>]+)>)/ig, '').substring(0, 100) : meta.description
                meta.imagePath = json.imageHash !== null ? `${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${json.imageHash}` : meta.imagePath
            }
        }

        await initialize(ctx);
        return {
            pageProps: {
                // Call page-level getInitialProps
                ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
                // Some custom thing for all pages
                pathname: ctx.pathname,
                title: meta.title,
                description: meta.description,
                imagePath: meta.imagePath
            },
        };

    };


    render() {
        let allowed = false;
        const { router } = this.props;
        if (router.pathname.startsWith("/admin")) {
            allowed = true;
        }
        const { Component, pageProps } = this.props;

        const LayoutComponent = allowed ? AdminLayout : DefaultLayout;

        return (
            <>
                <Head>
                    <link rel="shortcut icon" href="/images/favicon.ico" />
                    <title>Frontlinecheck</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    <meta name="og:title" content={pageProps.title} />
                    <meta name="og:description" content={pageProps.description} />
                    <meta name="og:image" content={pageProps.imagePath} />
                    <meta name="twitter:title" content={pageProps.title} />
                    <meta name="twitter:description" content={pageProps.description} />
                    <meta name="twitter:image" content={pageProps.imagePath} />
                </Head>

                <LayoutComponent>
                    <div className="content-wrap">
                        <Component {...pageProps} />
                    </div>
                </LayoutComponent>
            </>
        )
    }
}

export default wrapper.withRedux(withRouter(MyApp));






