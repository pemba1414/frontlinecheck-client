import { useState } from 'react';
import { Form, Input, InputGroup, InputGroupText } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faEnvelope, faUnlock } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';
import Router from 'next/router';

const SignUpPage = (): React.ReactElement => {
	const [email, setEmail] = useState<string>("")
	const [name, setName] = useState<string>("")
	const [signupError, setSignUpError] = useState<string>("")
	const [password, setPassword] = useState<string>("")

	const formSubmit = (e) => {
		e.preventDefault();
		axios.post(`/api/auth/register`,
			{ name, email, password },
			{ headers: { "Content-Type": "application/json" } })
			.then((response) => {
				setSignUpError(null)
				Router.push('/authorized/login')
			}
			)
			.catch(() => setSignUpError('Cannot register Users'))
	}
	return (
		<div className="limiter">
			<div className="container-login100">
				<div className="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
					<Form className="login100-form validate-form" onSubmit={formSubmit}>
						<span className="login100-form-title p-b-55">
							Register
					</span>

						<div className="wrap-input100  m-b-16">
							<Input className="input100 login-input" type="text" name="name" placeholder="Name" onChange={(e) => setName(e.target.value)} />
							<span className="focus-input100"></span>
							<span className="symbol-input100">
								<span className="lnr lnr-user">
									<FontAwesomeIcon icon={faUser} />
								</span>
							</span>
						</div>

						<div className="wrap-input100  m-b-16">
							<Input className="input100 login-input" type="text" name="email" placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
							<span className="focus-input100"></span>
							<span className="symbol-input100">
								<span className="lnr lnr-envelope">
									<FontAwesomeIcon icon={faEnvelope} />
								</span>
							</span>
						</div>

						<div className="wrap-input100  m-b-16">
							<Input className="input100 login-input" type="password" name="pass" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
							<span className="focus-input100"></span>
							<span className="symbol-input100">
								<span className="lnr lnr-lock">
									<FontAwesomeIcon icon={faUnlock} />
								</span>
							</span>
						</div>


						<div className="container-login100-form-btn p-t-25">
							<button className="login100-form-btn" type='submit'>
								Register
						</button>
						</div>

						<div className="text-center w-full p-t-55">
							<span className="txt1">
								Already a member? 
						</span>

							<a className="txt1 bo1 hov1" href="/authorized/login">
								  Log In
						</a>
						</div>
					</Form>
				</div>
			</div>
		</div>

	)
}

export default SignUpPage;