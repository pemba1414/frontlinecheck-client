import React from 'react'
import AdminHome from '../../components/admin/admin-home/home'
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../redux/auth.slice";
import UserInfoTable from '../../components/admin/user-info-table/UserInfoTable'

const Home = (): React.ReactElement => {
  const role: string = useSelector(AuthenticationSlice.getRole)
  return (
    <div>
      {
        role === 'ROLE_ADMIN' ?
          <UserInfoTable /> : <AdminHome />
      }
    </div>
  )
}

export default Home
