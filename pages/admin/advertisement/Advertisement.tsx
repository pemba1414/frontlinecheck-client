import { Form, Input, Label, Button, Row, Col } from 'reactstrap'
import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKeyboard, faLink } from '@fortawesome/free-solid-svg-icons'
import * as AuthenticationSlice from "../../../redux/auth.slice";
import axios from 'axios';
import { useSelector } from 'react-redux';
import Banner from '../../../components/banner/Banner'
import Tile from '../../../components/tile/Tile'
import { AdDetails } from '../../../types/addetails.type'


const Advertisments = () => {
    const token = useSelector(AuthenticationSlice.getToken)
    const [name, setName] = useState('');
    const [bannerAd, setBannerAd] = useState<AdDetails[]>([]);
    const [tileAd, setTileAd] = useState<AdDetails[]>([]);
    const [link, setLink] = useState('');
    const [file, setFile] = useState();
    const [type, setType] = useState('');

    let Ads: AdDetails[] = []

    const fetchApi = () => {
        axios.get(`/api/v2/adv/list`)
            .then((response) => {
                response.data.map((val: AdDetails, index) => {
                    Ads.push({ type: val.type, hash: val.hash })
                })

                let BannerAd: AdDetails[] = Ads.filter((ad) => ad.type === 'BANNER')
                setBannerAd(BannerAd)


                let tileAd: AdDetails[] = Ads.filter((ad) => ad.type === 'TILE')
                setTileAd(tileAd)
            })
            .catch((error) => console.error(error))
    }

    useEffect(() => {
        fetchApi()
    }, [])

    const submitForm = (e) => {
        e.preventDefault();
        const formdata = new FormData();

        formdata.append('file', file);

        formdata.append('data', JSON.stringify({
            name: name,
            link: link,
            type: type
        }))

        axios.post(`/api/v2/adv/create`, formdata, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'content-type': 'multipart/form-data'
            }
        })
            .then((response) => {
                console.log(response)
            })
            .catch((error) => console.error(error))
    }

    const handleChange = (e) => {
        setFile(e.target.files[0])
    }

    return (
        <div>
            <Form onSubmit={submitForm}>
                <Row>
                    <Col md={4}>
                       <div className="ad-select-area">
                       <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Name" onChange={(e) => setName(e.target.value)} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-keyboard">
                                    <FontAwesomeIcon icon={faKeyboard} />
                                </span>
                            </span>
                        </div>
                        <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Link" onChange={(e) => setLink(e.target.value)} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-llink">
                                    <FontAwesomeIcon icon={faLink} />
                                </span>
                            </span>
                        </div>
                        <Label>Image Type</Label>
                        <div className="wrap-input100  m-b-16 select-img">
                            <select onChange={(e) => setType(e.target.value)}>
                                <option>Select Image Type</option>
                                <option value="Banner (1240x100)">Banner(1240*100)</option>
                                <option value="Tile (300x250)">Tile(300*250)</option>
                            </select>
                        </div>
                        <div className="file-btn">
                        <Input type="file" placeholder="Image Upload" onChange={handleChange} />
                        </div>
                        <div className="btns">
                            <Button color="primary" type="submit">Add Selected</Button>
                        </div>
                       </div>
                    </Col>
                    <Col md={8}>
                        <h4>Banner Advertisements</h4>
                        <Banner banners={bannerAd} />
                        <h4>Tile Advertisements</h4>
                        <Tile tiles={tileAd} />
                    </Col>
                </Row>
            </Form>
        </div>
    )
}

export default Advertisments
