import { Button, Input, Form, Label } from 'reactstrap'
import { useRouter } from 'next/router'
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../../redux/auth.slice";
import axios from 'axios';
import { useEffect, useState } from 'react'
import Image from 'next/image'
import Router from 'next/router'

const EditWebsite = () => {
    const [name, setName] = useState<string>('')
    const [url, setUrl] = useState<string>('')
    const [slug, setSlug] = useState<string>('')
    const [slugType, setSlugType] = useState<string>('')
    const [slugAction, setSlugAction] = useState<string>('')
    const [inputSlug, setInputSlug] = useState<string>('')
    const [tagStart, setTagStart] = useState<string>('')
    const [tagEnd, setTagEnd] = useState<string>('')
    const [file, setFile] = useState<string>('')

    const token: string = useSelector(AuthenticationSlice.getToken)

    const router = useRouter()
    const { id } = router.query

    const handleChangeFile = (event) => {
        setFile(event.target.files[0])
    }

    const fetchApi = () => {
        axios.get(`/api/v2/website/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                console.log(response)
                setName(response.data.name)
                setUrl(response.data.url)
            })
            .catch((error) => console.error(error))
    }

    useEffect(() => {
        fetchApi()
    }, [])

    const selectSlug = (event): void => {
        setSlug(event.target.value)
    }

    const selectSlugType = (event): void => {
        setSlugType(event.target.value)
    }

    const selectAction = (event): void => {
        setSlugAction(event.target.value)
    }

    const formSubmit = (e) => {
        e.preventDefault()
        axios.post(`/api/v2/website/${id}/update`,{
            name, url
        }, {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }

        }).then((response) => {
            if (response) {
                Router.push('/admin/home')
            }
        })
        .catch((error) => console.error(error))
    }
    const imageSubmit = (event) => {
        event.preventDefault();

        const formdata = new FormData();
        formdata.append('file', file);

        axios.put(`/api/v2/${id}/updateImage`, formdata, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if (response) {
                    Router.push('/admin/home')
                }
            })
            .catch((error) => console.error(error))
    }
    return (
        <div>
            <div>
                <h1>Website Details</h1>
                <Form onSubmit={formSubmit}>
                    <Label>Name: </Label>
                    <Input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                    <Label>URL: </Label>
                    <Input type="text" value={url} onChange={(e) => setUrl(e.target.value)}/>
                    
                    <Label>Featured:</Label>
                    
                    <Button type="submit" color="success">Update</Button>
                    <Button type="submit" color="danger">Cancel</Button>
                    </Form>
            <Form onSubmit={imageSubmit}>
            <Label>Image:</Label>
                        <>
                            <Image
                                height={2}
                                width={2}
                                layout="responsive"
                                loading="lazy"
                                alt="image"
                                src={'/images/600x315-frontline-01.png'}
                                className="image-fluid"
                            />
                        </>
                     <Input type="file" placeholder="Image Upload" onChange={handleChangeFile}/>
                    <Button color="success" type="submit">Upload</Button>
            </Form>

            </div>
            <div>
                <h4>Add Slug</h4>
                <Form>
                    <select placeholder="Select Action" onChange={selectSlug}>
                        <option value="Search">Search</option>
                        <option value="Ignore">Ignore</option>
                    </select>
                    <select placeholder="Select Slug Type" onChange={selectSlugType}>
                        <option value="Starts With">Starts With</option>
                        <option value="Ends With">Ends With</option>
                        <option value="Contains">Contains</option>
                    </select>
                    <h5>Slug</h5>
                    <Input type="text" placeholder="Slug" onChange={(e) => setInputSlug(e.target.value)} />
                    <Button type="submit" color="success">Add</Button>
                </Form>
            </div>
            <div>
                <h3>Search Scopes</h3>
                <h2>Add Search Scope</h2>
                <Form>
                    <select placeholder="Add Action" onChange={selectAction}>
                        <option>Search</option>
                        <option>Ignore</option>
                    </select>
                Tag Start
               <Input type="text" placeholder="Tag Start" onChange={(e) => setTagStart(e.target.value)}/>
                Tag End
               <Input type="text" placeholder="End Start" onChange={(e) => setTagEnd(e.target.value)} />
                    <Button type="submit" color="success">Add</Button>
                </Form>
            </div>
        </div>
    )
}

export default EditWebsite;