import React, { useState, useEffect } from 'react';
import { Button, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../redux/auth.slice";
import axios from 'axios';
import Router from 'next/router'

type Details = {
    id: string,
    name: string
}

const AddNews = (callback) => {

    const token = useSelector(AuthenticationSlice.getToken)
    const [title, setTitle] = useState('')
    const [url, setUrl] = useState('')
    const [websiteId, setWebsiteId] = useState<number>();
    const [keywordIdentifier, setKeywordIdentifier] = useState('')
    const [featured, setFeatured] = useState(false);
    const [keywords, setKeywords] = useState([]);
    const [sites, setSites] = useState([])

    const fetchWebsite = () => {
        axios.get(`/api/v2/userWebsite/list`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                setSites(response.data.userWebsites)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    let keywordList: Details[] = [];

    const fetchList = () => {
        axios.get(`/api/v2/keyword/list`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                response.data.map((value: Details, index) =>
                    keywordList.push({ id: 'K' + '_' + value.id, name: value.name })
                )
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const fetchGroupKeyword = () => {
        axios.get('/api/v2/keywordGroup/list', {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                response.data.map((value: Details, index) =>
                    keywordList.push({ id: 'KG' + '_' + value.id, name: value.name })
                )
                setKeywords(keywordList)
            })
            .catch((error) => console.error(error))
    }

    const formSubmit = (e) => {
        e.preventDefault()
        axios.post(`/api/v2/searchResult/addManually`, {
            title, url, websiteId, keywordIdentifier, featured
        }, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if(response){
                    Router.push('/admin/home')
                }
            })
            .catch((error) => console.log(error))
    }

    useEffect(() => {
        fetchWebsite(), fetchList(), fetchGroupKeyword()
    }, [])



    const handleChange = (event) => {
        if (event.target.checked) {
            setFeatured(true)
        } else {
            setFeatured(false)
        }
    }

    const handleWebsite = (e) => {
        const num = Number(e.target.value)
        setWebsiteId(num)
    }

    const handleKeywordIdentifier = (e) => {
        setKeywordIdentifier(e.target.value)
    }
    return (
        <Row>
            <Col md={12}>
                <span className="padding-30">
                    <h3>Add News Manually</h3>
                    <br></br>
                    <br></br>
                    <Form onSubmit={formSubmit}>
                        <FormGroup>
                            <Label for="newsTitle">Title</Label>
                            <Input type="text" name="heading" id="newsHeading" placeholder="" onChange={(e) => setTitle(e.target.value)} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="newsUrl">Url</Label>
                            <Input type="text" name="heading" id="newsHeading" placeholder="" onChange={(e) => setUrl(e.target.value)} />
                        </FormGroup>
                        <FormGroup>
                            <Label>Website</Label>
                            <Input type="select" name="select" id="exampleSelect" className="form-control" onChange={handleWebsite}>
                                <option>Select Website</option>
                                {
                                    sites && sites.map((val, index) => {
                                        return <option value={val.id} key={index}>{val.name}</option>
                                    }
                                    )}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleSelect">Keyword</Label>
                            <Input type="select" name="select" id="exampleSelect" onChange={handleKeywordIdentifier}>
                                <option>Select Keyword</option>
                                {keywords && keywords.map((keyword, index) => {
                                    return (
                                        <option key={index} value={keyword.id}>{keyword.name}</option>
                                    )
                                })}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label>
                                Featured
                            </Label>
                            <Input type="checkbox" className="checkbox-label" onChange={handleChange} />
                        </FormGroup>
                        <Button color="success">Submit</Button>
                    </Form>
                </span>
            </Col>
        </Row>
    )
}
export default AddNews