import React, { useEffect } from 'react';
import { Button, Col, Form, FormGroup, FormText, Input, Label, Row } from 'reactstrap';
import Image from 'next/image'
import { useState } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../../redux/auth.slice";
import axios from 'axios';
import Router from 'next/router'
import { useRouter } from 'next/router'


const UpdateNews = () => {

    const token = useSelector(AuthenticationSlice.getToken)
    const router = useRouter()
    const { id } = router.query

    const [status, setStatus] = useState('')
    const [file, setFile] = useState('');
    const [title, setTitle] = useState('')
    const [image, setImage] = useState('')
    const [comments, setComments] = useState('')
    const [featured, setFeatured] = useState<boolean>(null)

    const handleEditorChange = (content, editor) => {
        setComments(content);
    }

    const fetchApi = () => {
        axios.get(`/api/v2/searchResult/get/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                setComments(response.data.comments)
                setStatus(response.data.approvalStatus)
                if (response.data.approvalStatus === 'Pending') {
                    setStatus('Approved with note')
                }
                setFeatured(response.data.featured)
                setTitle(response.data.title)
                setImage(response.data.hash)
            })
            .catch((error) => console.error(error))
    }

    useEffect(() => {
        fetchApi()
    }, [])

    const submitForm = (e) => {
        e.preventDefault();
        axios.put(`/api/v2/searchResult/${id}/approvalStatus/update`,
            {
                title, comments, featured, status
            }, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => Router.push('/admin/home'))
            .catch((error) => console.error(error))
       
    }

    const imageSubmit = (e) => {
        e.preventDefault();

        const formdata = new FormData();
        formdata.append('file', file);

        axios.put(`/api/v2/searchResult/${id}/updateImage`, formdata, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                if (response) {
                    Router.push('/admin/home')
                }
            })
            .catch((error) => console.error(error))
    }

    const handleChange = (event) => {
        if (event.target.checked) {
            setFeatured(true)
        } else {
            setFeatured(false)
        }
    }

    const handleChangeFile = (e) => {
        setFile(e.target.files[0])
    }

    return (
        <Row>
            <Col md={12}>
                <span className="padding-30">
                        <Row>
                            <Col md={4}>
                                <h5 className="m-b-20">Image</h5>
                                <Form onSubmit={imageSubmit}>
                                    <div>
                                        {
                                            image === null ? '' :
                                                <>
                                                    <Image
                                                        height={20}
                                                        width={20}
                                                        layout="responsive"
                                                        loading="lazy"
                                                        src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${image}`}
                                                        className="image-fluid"
                                                    />

                                                </>
                                        }

                                    </div>
                                    <FormGroup>
                                        <div className="wrap-input100 m-b-30 m-t-20">
                                            <Input type="file" placeholder="Image Upload" onChange={handleChangeFile} />
                                            <FormText color="muted">
                                                Upload an image of size 3 MB or less
                                            </FormText>
                                        </div>
                                    </FormGroup>
                                    <Button color="primary" type="submit">Update</Button>
                                </Form>
                            </Col>

                            
                            <Col md={7}>
                            <Form onSubmit={submitForm}>
                                <Row>
                                    <Col md={4}>
                                        <Label for="exampleSelect">Approval Status</Label>
                                        <Input type="select" name="select" id="exampleSelect" value={status} onChange={(e) => setStatus(e.target.value)}>
                                            <option value="Pending">Pending</option>
                                            <option value="Approved">Approved</option>
                                            <option value="Declined">Declined</option>
                                            <option value="Approved with note">Approved with note</option>
                                        </Input>
                                    </Col>

                                    <Col md={6}>
                                        <div className="news-title-space">
                                            <Label for="newsHeading">Heading</Label>
                                            <Input type="text" name="heading" id="newsHeading" value={title} placeholder="" onChange={(e) => setTitle(e.target.value)} />
                                        </div>
                                    </Col>

                                    <Col md={2}>
                                        <div className="form-checkbox-a">
                                            <Input type="checkbox" className="checkbox-label" onChange={handleChange} />
                                            <Label> Featured </Label>
                                        </div>
                                    </Col>

                                    <Col md={12}>
                                        <div className="statement">
                                            <Label for="newsHeading">Statement</Label>
                                            <Editor
                                                value={comments}
                                                apiKey="z7in8ajytyg87b05cogu5pp9708oxbd3r8bkhgmlcw6g7fuz"

                                                init={{
                                                    height: 500,
                                                    menubar: false,
                                                    plugins: [
                                                        'advlist autolink lists link image charmap print preview anchor',
                                                        'searchreplace visualblocks code fullscreen',
                                                        'insertdatetime media table paste code help wordcount'
                                                    ],
                                                    toolbar:
                                                        'undo redo | formatselect | image | bold italic backcolor | \
                                        alignleft aligncenter alignright alignjustify | \
                                        bullist numlist outdent indent | removeformat | help',

                                                    file_picker_callback: function (callback, value, meta) {
                                                        var input = document.createElement('input');
                                                        input.setAttribute('type', 'file');
                                                        input.setAttribute('accept', 'image/*');
                                                        input.onchange = function () {
                                                            var file = input.files[0];

                                                            var reader = new FileReader();
                                                            reader.onload = function () {
                                                                /*
                                                                  Note: Now we need to register the blob in TinyMCEs image blob
                                                                  registry. In the next release this part hopefully won't be
                                                                  necessary, as we are looking to handle it internally.
                                                                */
                                                                var id = 'blobid' + (new Date()).getTime();
                                                                // @ts-ignore
                                                                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                                                var base64 = (reader.result as string).split(',')[1];
                                                                var blobInfo = blobCache.create(id, file, base64);
                                                                blobCache.add(blobInfo);

                                                                /* call the callback and populate the Title field with the file name */
                                                                callback(blobInfo.blobUri(), { title: file.name });
                                                            };
                                                            reader.readAsDataURL(file);
                                                        };
                                                        input.click();
                                                    }
                                                }}
                                                onEditorChange={handleEditorChange}
                                            />
                                        </div>
                                    </Col>
                        <div className="sub-btn">            
                        <Button color="primary">Submit</Button>
                        </div>
                                </Row>

                                </Form>
                            </Col>
                        
                        </Row>

                   

                </span>
            </Col>
        </Row>
    )
}
export default UpdateNews