import { Form, Label, Input, Button, InputGroup, InputGroupText, } from 'reactstrap'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKeyboard, faGlobe } from '@fortawesome/free-solid-svg-icons';
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../../redux/auth.slice";

const Search = () => {

    const token = useSelector(AuthenticationSlice.getToken)
    const [sites, setSites] = useState([])
    const [keyword, setKeyword] = useState([])

    const fetchWebsite = () => {
        axios.get(`/api/v2/userWebsite/list`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                setSites(response.data.userWebsites)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    useEffect(() => {
        fetchWebsite()
    }, [])

    const handleWebsite = (e) => {
        setSites(e.target.value)
    }

    const formSubmit = (e) => {
        e.preventDefault();

    }

    const handleKeyword = (e) => {
        setKeyword(e.target.value)
    }

    return (
        <div>
            <h1>Search for a text in your desired website</h1>
            <Form onSubmit={formSubmit}>
                <Label>Website</Label>
                <InputGroup className="input-group1">
                    <InputGroupText>
                        <FontAwesomeIcon icon={faGlobe} />
                    </InputGroupText>
                    <Input type="select" name="select" id="exampleSelect" className="form-control" onChange={handleWebsite}>
                        <option>Select Website</option>
                        {
                            sites && sites.map((val, index) => {
                                return <option value={val.id} key={index}>{val.name}</option>
                            }
                            )}
                    </Input>
                </InputGroup>

                <InputGroup className="input-group1">
                    <InputGroupText>
                        <FontAwesomeIcon icon={faKeyboard} />
                    </InputGroupText>
                    <Input type="text" name="keyword" onChange={handleKeyword} />
                </InputGroup>

                <Button type="submit" >Search</Button>
            </Form>
        </div>
    )
}

export default Search;