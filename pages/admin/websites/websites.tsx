import React, { useState, useEffect} from 'react'
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../../redux/auth.slice";
import SuperAdminWebsites from '../../../components/super-admin/websites/Websites'
import AdminWebsites from '../../../components/admin/website-list/AdminWebsite'

const Websites = (): React.ReactElement => {
  const role: string = useSelector(AuthenticationSlice.getRole)
  
  return (
      <div className="website-warp">
        {
        role === 'ROLE_ADMIN' ?
          <SuperAdminWebsites /> : <AdminWebsites />
      }
      </div>
  )
}

export default Websites;
