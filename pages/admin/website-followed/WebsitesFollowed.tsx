import {Button} from 'reactstrap';
import {useState} from 'react';

const WebsitedFollowed = () => {
    const websites = ['nayapatrika', 'Frontline Khabar', 'Online Khabar']

    let [websiteList, setWebsiteList] = useState([]);
    let [selectedWebsite, setSelectedWebsite] = useState([]);

   const formSubmit = (e) => {
       e.preventDefault();
       setSelectedWebsite([...selectedWebsite, websiteList]);
   }

   const selectWebsite = (e) => {
       if(e.target.checked){
           websiteList.push(e.target.name);
       }
       else {
        websiteList = websiteList.filter(x => x != e.target.name)
      }
   }

    return(
        <div>
            <h1>Websites Followed</h1>
            {
                selectedWebsite.length === 0 ? <h1>No websited Followed</h1> : 
                    selectedWebsite.map((val, index) => 
                        <h1 key="index">{val}</h1>)
                
            }
            
        <form onSubmit={formSubmit}>
           {
               websites.map((val, index) => 
               <div key="index">
                    <input type="checkbox" onChange={selectWebsite} name={val} /> 
                    <label>{val}</label>
                </div>
               )
            }
            <Button type="submit">Follow Selected</Button>
            </form>
        </div>
    )
}

export default WebsitedFollowed;