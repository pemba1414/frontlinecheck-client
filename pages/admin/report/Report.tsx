import { useState } from 'react';
import {Nav, NavItem, NavLink, TabPane, TabContent} from 'reactstrap'
import SearchStatus from '../../../components/admin/reports/SearchStatus'
import SearchResults from '../../../components/admin/reports/SearchResults'
import ScrappedLinks from '../../../components/admin/reports/ScrappedLinks'
import LinksByWebsite from '../../../components/admin/reports/LinksByWebsite'
import ErrorsinLinks from '../../../components/admin/reports/ErrorsInLinks'

const Report = () => {
    const [activeTab, setActiveTab] = useState('result')

    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    return(
        <div>
            <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'result') ? 'active' : ''}
                            aria-expanded="true"
                            onClick={() => {
                                toggle('result');
                            }}>
                            Search Result 
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'status') ? 'active' : ''}
                            onClick={() => {
                                toggle('status');
                            }}>
                            Search Status
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'links') ? 'active' : ''}
                            onClick={() => {
                                toggle('links');
                            }}>
                            Links By Website
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'errors') ? 'active' : ''}
                            onClick={() => {
                                toggle('errors');
                            }}>
                            Errors In Links 
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'scrapped') ? 'active' : ''}
                            onClick={() => {
                                toggle('scrapped');
                            }}>
                            Scrapped Links
                        </NavLink>
                    </NavItem>
                </Nav>

                <TabContent activeTab={activeTab}>
                    <TabPane tabId="result">
                           <SearchResults />
                    </TabPane>
               
                    <TabPane tabId="status">
                            <SearchStatus />
                    </TabPane>
               
                    <TabPane tabId="links">
                            <LinksByWebsite />
                    </TabPane>

                    <TabPane tabId="errors">
                            <ErrorsinLinks />
                    </TabPane>
            
                    <TabPane tabId="scrapped">
                            <ScrappedLinks />
                    </TabPane>
                </TabContent>
        </div>
    )

}

export default Report;