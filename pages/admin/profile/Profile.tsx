import { Form, Input, Label, Button,Row, Col } from 'reactstrap'
import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKeyboard, faLink } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';
import Image from 'next/image';
import * as AuthenticationSlice from "../../../redux/auth.slice";
import { useSelector } from 'react-redux';



const Profile = () => {
    const token = useSelector(AuthenticationSlice.getToken)
    const [name, setName] = useState('');
    const [profilePictureHash, setProfilePictureHash] = useState('')
    const [email, setEmail] = useState('');
    const [website, setWebsite] = useState('');
    const [contactNumber, setContactNumber] = useState('');
    const [file, setFile] = useState('');
    const [bio, setBio] = useState('');



    const fetchApi = () => {
        axios.get(`/api/v2/profile/`,  {headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }})
            .then((response) => {
                setName(response.data.name)
                setEmail(response.data.email)
                setProfilePictureHash(response.data.profilePictureHash)
                setContactNumber(response.data.contactNumber)
                setBio(response.data.bio)
                setWebsite(response.data.website)
            })
            .catch((error) => console.error(error))
    }

    useEffect(() => {
        fetchApi()
    },[])


    const submitForm = (e) => {
        e.preventDefault();
        const formdata = new FormData();
        formdata.append('file', file);

        formdata.append('data', JSON.stringify({
            name: name,
            email: email,
            website: website,
            contactNumber: contactNumber,
            bio: bio
        }))

        axios.post(`/api/v2/profile/createOrUpdate`, formdata , {
            headers: {
                'Authorization': 'Bearer ' + token,
                'content-type': 'multipart/form-data'
            }
        })
            .then((response) => console.log(response))
            .catch((error) => console.error(error))
    }

    const handleChange = (e) => {
        setFile(e.target.files[0])
    }

    return (
        <div>
            <Form onSubmit={submitForm}>
                <Row>
                <Col md={{ size:3, offset: 1 }}>
                        <div className="image-here">
                            <Image
                                height={30}
                                width={30}
                                layout="responsive"
                                loading="lazy"
                                src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${profilePictureHash}`}
                                alt="Profile image here"
                            />
                            <Label>Change Profile Picture</Label>
                            <Input type="file" placeholder="Image Upload" onChange={handleChange} />
                        </div>
                    </Col>
                    
                    <Col md={6}>
                        <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Name" onChange={(e) => setName(e.target.value)} value={name} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-keyboard">
                                    <FontAwesomeIcon icon={faKeyboard} />
                                </span>
                            </span>
                        </div>

                        <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Email" onChange={(e) => setEmail(e.target.value)} value={email} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-keyboard">
                                    <FontAwesomeIcon icon={faLink} />
                                </span>
                            </span>
                        </div>

                        <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Website" onChange={(e) => setWebsite(e.target.value)} value={website} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-keyboard">
                                    <FontAwesomeIcon icon={faLink} />
                                </span>
                            </span>
                        </div>

                        <div className="wrap-input100  m-b-16">
                            <Input type="text" className="input100" placeholder="Contact Number" onChange={(e) => setContactNumber(e.target.value)} value={contactNumber} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100">
                                <span className="lnr lnr-keyboard">
                                    <FontAwesomeIcon icon={faLink} />
                                </span>
                            </span>
                        </div>

                        <div className="wrap-input100  m-b-16">
                            <Input type="textarea" placeholder="Enter your Bio" onChange={(e) => setBio(e.target.value)} value={bio} />
                            <span className="focus-input100"></span>
                        </div>

                        <Button color="primary">Update</Button>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}

export default Profile;
