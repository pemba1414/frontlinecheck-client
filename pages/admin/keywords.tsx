import React, {useState, useEffect} from 'react'
import {
    Button, Col, Form, FormGroup,
    Input, Nav, NavItem, NavLink, Row, TabContent, TabPane
} from 'reactstrap'
import KeywordsList from '../../components/admin/keyword-list';
import {useSelector} from "react-redux";
import * as AuthenticationSlice from "../../redux/auth.slice";
import axios from 'axios';
import {KeywordType} from '../../types/keyword.type';
import ConfirmationModal from '../../components/confirmation-modal/ConfirmationModal';
import Select from 'react-select';

const Keywords = (): React.ReactElement => {

    const token = useSelector(AuthenticationSlice.getToken)
    const [openModal, setOpenModal] = useState(false)
    const [activeTab, setActiveTab] = useState('keyword')
    const [keyword, setKeyword] = useState('')
    const [keywordList, setKeywordList] = useState<KeywordType[]>([])
    const [featuredKeywordList, setFeaturedKeywordList] = useState<KeywordType[]>([])
    const [nonFeaturedKeywordList, setNonFeaturedKeywordList] = useState<KeywordType[]>([])
    const [keywordGroupList, setKeywordGroupList] = useState<KeywordType[]>()
    const [featuredKeywordObjForAdd, setFeaturedKeywordObjForAdd] = useState([])
    const [nonFeaturedKeywordGroupObjForAdd, setNonFeaturedKeywordGroupObjForAdd] = useState([])

    const [featuredKeyGroupList, setFeaturedKeyGroupList] = useState<KeywordType[]>([])
    const [nonFeaturedKeywordGroupList, setNonFeaturedKeywordGroupList] = useState<KeywordType[]>([])

    const [title, setTitle] = useState('')
    const [deleteMsg, setDeleteMsg] = useState('')
    const [deleteKeyID, setDeleteKeyID] = useState()
    const [deleteListName, setDeleteListName] = useState('')
    const [deleteKey, setDeleteKey] = useState(false)
    const [keywordID, setKeywordID] = useState([])

    //switching tabs
    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    // keyword related functions
    const updateKeywordList = (response) => {
        let initKeyList: KeywordType[] = [];
        response.data.map((data: KeywordType, index) => {
            initKeyList.push(data)
        })
        setKeywordList(initKeyList)
    }
    const getKeywordList = () => {
        axios.get(`/api/v2/keyword/list`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                updateKeywordList(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    const createKeyword = (newKeyword) => {
        axios.post('/api/v2/keyword/create', {name: newKeyword.trim()}, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                setKeyword('')
                setKeywordList(keywordList => [...keywordList, response.data])
            })
            .catch((error) => {
                console.error(error)
            })
    }
    const addKeywordList = (newKeyword) => {
        if (newKeyword.length > 0) {
            let tempKeyword = [];
            keywordList.map((data, index) => {
                tempKeyword.push(data.name)
            })
            if (!tempKeyword.includes(newKeyword)) {
                createKeyword(newKeyword)
            } else {
                setTitle('Error');
                setDeleteMsg('Keyword exists!!! Please enter a new keyword');
                setDeleteKey(false);
                setOpenModal(true);
                setKeyword('');
            }
        }else {
            setTitle('Error');
            setDeleteMsg('Please enter a new keyword');
            setDeleteKey(false);
            setOpenModal(true);
            setKeyword('');
        }
    }
    const deleteKeywordList = (id) => {
        axios.put(`/api/v2/keyword/${id}/delete`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                getKeywordList()
            })
            .catch((error) => {
                console.log(error)
            })
    }

    // featured keywords related functions
    const updateFeaturedKeywordList = (response) => {
        const initFeaturedKeywordList = [];
        response.data.featuredKeywords.map((data: KeywordType, index) => {
            initFeaturedKeywordList.push(data)
        })
        setFeaturedKeywordList(initFeaturedKeywordList)
    }
    const updateNonFeaturedKeywordList = (response) => {
        const initNonFeaturedKeywordList = [];
        response.data.nonFeaturedKeywords.map((data: KeywordType, index) => {
            initNonFeaturedKeywordList.push(data)
        })
        setNonFeaturedKeywordList(initNonFeaturedKeywordList)
    }
    const getFeaturedKeywordList = () => {
        axios.get(`/api/v2/keyword/listByFeatured`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                updateFeaturedKeywordList(response)
                updateNonFeaturedKeywordList(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    const addFeaturedKeywordList = (data) => {
        const requestOption = []
        if (data.length + featuredKeywordList.length <= 3) {
            data.map((value, id) => {
                requestOption.push({id: value.value, name: value.label, featured: false})
            })
            axios.put('/api/v2/keyword/addToFeaturedList', requestOption, {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {

                    requestOption.map((data, index) => {
                        setFeaturedKeywordList(featuredKeywordList => [...featuredKeywordList, data])
                    })

                })
                .catch((error) => {
                    console.error(error)
                })
        } else {
            setTitle('Featured Limit Exceeded');
            setDeleteMsg('Only upto 3 keyword groups can be set as featured.');
            setDeleteKey(false);
            setOpenModal(true)
        }
    }
    const handleAddFeaturedKeywordList = (newFeaturedKeywordObject) => {
        if (newFeaturedKeywordObject.length != 0) {
            if (featuredKeywordList != undefined) {
                if (featuredKeywordList.length <= 2) {
                    addFeaturedKeywordList(newFeaturedKeywordObject)
                    //--> clear the input field here <--
                }
            } else {
                addFeaturedKeywordList(newFeaturedKeywordObject)
            }
        }
    }
    const deleteFeaturedKeywordList = (id) => {
        axios.put(`/api/v2/keyword/${id}/removeFromFeaturedList`)
            .then((response) => {
                getFeaturedKeywordList()
            })
            .catch((error) => {
                console.log(error)
            })
    }

    //featured group keywords related functions
    const updateKeywordGroupList = (response) => {
        const initKeywordGroupList = [];
        response.data.map((data: KeywordType, index) => {
            initKeywordGroupList.push(data)
        })
        setKeywordGroupList(initKeywordGroupList)
    }
    const getKeywordGroupList = () => {
        axios.get(`/api/v2/keywordGroup/list`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                updateKeywordGroupList(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    const addKeywordGroupList = (data) => {
        console.log(data.length)
        const requestOption = []
        if (data.length <= 4) {
            data.map((value, id) => {
                requestOption.push({id: value.value, name: value.label, featured: false})
            })
            axios.post('/api/v2/keywordGroup/create', requestOption, {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    getKeywordGroupList()
                })
                .catch((error) => {
                    console.error(error)
                })
        } else {
            setTitle('Allowed Number of Keywords Exceeded');
            setDeleteMsg('Only upto 4 keywords can be selected at a time.');
            setDeleteKey(false);
            setOpenModal(true)
        }
    }
    const handleAddKeywordGroupList = (newFeaturedKeywordGroupObject) => {
        if (newFeaturedKeywordGroupObject.length != 0) {
            if (keywordGroupList != undefined) {
                if (keywordGroupList.length <= 4) {
                    addKeywordGroupList(newFeaturedKeywordGroupObject)
                } else {
                    setTitle('Allowed Number of Keywords Exceeded');
                    setDeleteMsg('Only upto 5 keywords can be selected at a time.');
                    setDeleteKey(false);
                    setOpenModal(true)
                }
            } else {
                addKeywordGroupList(newFeaturedKeywordGroupObject)
            }
        }
    }
    const deleteKeywordGroupList = (id) => {
        axios.put(`/api/v2/keywordGroup/${id}/delete`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                getKeywordGroupList()
            })
            .catch((error) => {
                console.log(error)
            })
    }

    // non featured keywords groups related functions
    const updateNonFeaturedKeywordGroupList = (response) => {
        const initNonFeaturedKeyGroupList = [];
        response.data.nonFeaturedKeywordGroups.map((data: KeywordType, index) => {
            initNonFeaturedKeyGroupList.push(data)
        })
        setNonFeaturedKeywordGroupList(initNonFeaturedKeyGroupList)
    }
    const getNonFeaturedKeywordsGroupList = () => {
        axios.get(`/api/v2/keywordGroup/listByFeatured`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                updateNonFeaturedKeywordGroupList(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }


    //featured keywords groups related function
    const updateFeaturedKeyGroupList = (response) => {
        const initFeaturedKeyGroupList = [];
        response.data.featuredKeywordGroups.map((data: KeywordType, index) => {
            initFeaturedKeyGroupList.push(data)
        })
        setFeaturedKeyGroupList(initFeaturedKeyGroupList)
    }
    const getFeaturedKeyGroupsList = () => {
        axios.get(`/api/v2/keywordGroup/listByFeatured`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                updateFeaturedKeyGroupList(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const addFeaturedKeyGroupsList = (data) => {
        const requestOption = []
        if (data.length + featuredKeyGroupList.length <= 3) {
            data.map((value, id) => {
                requestOption.push({id: value.value})
            })
            axios.post('/api/v2/keywordGroup/addToFeaturedList', requestOption, {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    getFeaturedKeyGroupsList()
                })
                .catch((error) => {
                    console.error(error)
                })
        } else {
            setTitle('Featured Limit Exceeded');
            setDeleteMsg('Only upto 3 keyword groups can be set as featured.');
            setDeleteKey(false);
            setOpenModal(true)
        }
    }
    const handleNonFeaturedKeywordsGroupList = (newNonFeaturedKeywordObject) => {
        if (newNonFeaturedKeywordObject.length != 0) {
            if (featuredKeyGroupList != undefined) {
                if (featuredKeyGroupList.length <= 2) {
                    addFeaturedKeyGroupsList(newNonFeaturedKeywordObject)
                } else {
                    setTitle('Featured Limit Exceeded');
                    setDeleteMsg('Only upto 3 keyword groups can be set as featured.');
                    setDeleteKey(false);
                    setOpenModal(true)
                }
            } else {
                addFeaturedKeyGroupsList(newNonFeaturedKeywordObject)
            }
        }
    }
    const deleteFeaturedKeywordGroupList = (id) => {
        axios.put(`/api/v2/keywordGroup/${id}/removeFromFeaturedList`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                getFeaturedKeyGroupsList()
            })
            .catch((error) => {
                console.log(error)
            })
    }

    //retrieve lists
    useEffect(() => {
        getKeywordList()
        getKeywordGroupList()
        getFeaturedKeywordList()
        getNonFeaturedKeywordsGroupList()
        getFeaturedKeyGroupsList()
    }, [])


    //callback values for deleting keyword based upon list name
    const removeKeyword = (id, name, listName) => {
        setDeleteKeyID(id);
        setDeleteListName(listName)
        setTitle('Confirmation');
        setDeleteMsg('Are you sure you want to delete the keyword group ' + name + '?');
        setDeleteKey(true);
        setOpenModal(true)
    }
    //deleting list element according to their corresponding list
    const confirmDelete = (deleteKey: boolean, deleteFrom: string) => {
        setOpenModal(false);
        if (deleteKey) {
            if (deleteFrom === "keywordList") {
                deleteKeywordList(deleteKeyID)
            } else if (deleteFrom === "featuredKeywordList") {
                deleteFeaturedKeywordList(deleteKeyID)
            } else if (deleteFrom === "keywordGroupList") {
                deleteKeywordGroupList(deleteKeyID)
            } else {
                deleteFeaturedKeywordGroupList(deleteKeyID)
            }
        }
    }

    // useEffect(() => {
    //     prepareKeywordForSelect('nonFeaturedKeywordGroup');
    // }, [nonFeaturedKeywordGroupList])
    // useEffect(() => {
    //     prepareKeywordForSelect('featuredKeywordGroup');
    //     prepareKeywordForSelect('nonFeaturedKeywordList');
    // }, [keywordGroupList])

    const prepareKeywordForSelect = (listOf) => {
        const keyForMap = [];
        if (listOf == "nonFeaturedKeywordGroup") {
            nonFeaturedKeywordGroupList.map((data, index) => {
                keyForMap.push({value: data.id, label: data.name})
            })
        } else if (listOf == "featuredKeyword" || listOf == "featuredKeywordGroup") {
            keywordList.map((data, index) => {
                keyForMap.push({value: data.id, label: data.name})
            })

        } else if (listOf == "featuredKeyword" || listOf == "nonFeaturedKeywordList") {
            keywordList.map((data, index) => {
                keyForMap.push({value: data.id, label: data.name})
            })
        }
        return (keyForMap)
    }


    const handleChange = (listObj, listOf) => {
        if (listOf == "featuredKeyword" || listOf == "featuredKeywordGroup") {
            setFeaturedKeywordObjForAdd(listObj)
        } else if (listOf == "nonFeaturedKeywordGroup") {
            setNonFeaturedKeywordGroupObjForAdd(listObj)

        }
    }
    return (
        <div className="jumbotron keyword-warp">
            <div>
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'keyword') ? 'active' : ''}
                            aria-expanded="true"
                            onClick={() => {
                                toggle('keyword');
                            }}>
                            Your Keywords
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={(activeTab === 'group') ? 'active' : ''}
                            onClick={() => {
                                toggle('group');
                            }}>
                            Your Keyword Groups
                        </NavLink>
                    </NavItem>
                </Nav>

                <TabContent activeTab={activeTab}>
                    <TabPane tabId="keyword">
                        <Form onSubmit={e => e.preventDefault()}>
                            <Row>
                                <Col md={4}>
                                    <FormGroup>
                                        <Input type="text" name="keyword" id="keywordNew"
                                               placeholder="Keywords"
                                               value={keyword}
                                               onChange={e => setKeyword(e.target.value)
                                               }/>
                                    </FormGroup>
                                </Col>
                                <Col md={4}>
                                    <div className="keyword-btn">
                                        <Button color="primary" onClick={() => addKeywordList(keyword)}>Add</Button>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                        <KeywordsList
                            list={keywordList}
                            callback={removeKeyword}
                            listName="keywordList"
                        />
                        <h4>Featured Keywords</h4>
                        <Form onSubmit={e => e.preventDefault()}>
                            <Row>
                                <Col md={6}>
                                    <FormGroup>
                                        <Select
                                            onChange={(value) => handleChange(value, "featuredKeyword")}
                                            closeMenuOnSelect={false}
                                            id="indexofFeaturedKeyword"
                                            isMulti
                                            name="featuredKeyword"
                                            options={prepareKeywordForSelect("nonFeaturedKeywordList")}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={4}>
                                    <div className="keyword-btn">
                                        <Button color="primary"
                                                onClick={() => handleAddFeaturedKeywordList(featuredKeywordObjForAdd)}>Add
                                            To Featured List</Button>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                        <KeywordsList
                            list={featuredKeywordList}
                            callback={removeKeyword}
                            listName="featuredKeywordList"
                        />
                    </TabPane>
                    <TabPane tabId="group">
                        <Row>
                            <Col md={4}>
                                <Form onSubmit={e => e.preventDefault()}>
                                    <FormGroup>
                                        <Select
                                            onChange={(value) => handleChange(value, "featuredKeywordGroup")}
                                            closeMenuOnSelect={false}
                                            id="indexofFeaturedKeywordGroup"
                                            isMulti
                                            name="featuredKeywordGroup"
                                            options={prepareKeywordForSelect("featuredKeywordGroup")}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </FormGroup>
                                </Form>
                            </Col>
                            <Col md={3}>
                                <div className="keyword-btn">
                                    <Button onClick={() => handleAddKeywordGroupList(featuredKeywordObjForAdd)}
                                            className="btn-primary">Add</Button>
                                </div>
                            </Col>
                        </Row>
                        <KeywordsList
                            list={keywordGroupList}
                            callback={removeKeyword}
                            listName="keywordGroupList"
                        />

                        <h4>Featured Keywords Groups</h4>
                        <Row>
                            <Col md={6}>
                                <Form onSubmit={e => e.preventDefault()}>
                                    <FormGroup>
                                        <Select
                                            onChange={(value) => handleChange(value, "nonFeaturedKeywordGroup")}
                                            closeMenuOnSelect={false}
                                            id="indexofNonFeaturedKeywordGroup"
                                            isMulti
                                            name="nonFeaturedKeywordGroup"
                                            options={prepareKeywordForSelect("nonFeaturedKeywordGroup")}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />

                                    </FormGroup>
                                </Form>
                            </Col>
                            <Col md={4}>
                                <div className="keyword-btn">
                                    <Button
                                        onClick={() => handleNonFeaturedKeywordsGroupList(nonFeaturedKeywordGroupObjForAdd)}
                                        color="primary">Add To Featured List</Button>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <KeywordsList
                                list={featuredKeyGroupList}
                                callback={removeKeyword}
                                listName="featuredKeyGroupList"
                            />
                        </Row>
                    </TabPane>
                </TabContent>
            </div>
            <>{openModal ?
                <ConfirmationModal
                    heading={title}
                    description={deleteMsg}
                    deleteKey={deleteKey}
                    deleteFrom={deleteListName}
                    callback={confirmDelete}
                />
                :
                " "}</>
        </div>
    )
}

export default Keywords
