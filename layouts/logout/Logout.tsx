import axios from 'axios'
import { useDispatch, useSelector } from "react-redux"
import * as AuthenticationSlice from "../../redux/auth.slice";
import { Button } from 'reactstrap';
import Router from 'next/router'

const Logout = () => {

    const token: string = useSelector(AuthenticationSlice.getToken)
    const secret: string = useSelector(AuthenticationSlice.getSecret)

    let buttonText: string = (secret !== null && secret !== 'null') ? 'Switch To Admin' : 'Log Out'

    const dispatch = useDispatch()
    const deAuthenticate = (): void => {
        if (secret !== null && secret !== 'null') {
            axios.post('/api/auth/switchBackToAdmin', { secret: secret }, {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json'
                }
            }).then((response) => {
                const token = response.data.accessToken
                const role = response.data.authorities[0].authority
                dispatch(AuthenticationSlice.authenticate({ token: token, role: role, secret: null }))
                Router.push('/admin/home')
            })
        } else {
            dispatch(AuthenticationSlice.deAuthenticate())
        }
    }
    return (
        <Button outline color="danger" className="log-out-btn" onClick={deAuthenticate}>{buttonText}</Button>
    )
}

export default Logout;