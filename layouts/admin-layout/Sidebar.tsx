import NavItem from "./../../components/admin/nav-item";
import {Button} from "reactstrap";
import Router from "next/router";
import * as React from "react";
import Link from 'next/link';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons'

const navItemsMap = {
    "Dashboard": {"iconClass": "icon-dashboard", "link": "/user/dashboard"},
    "Recent Booking": {"iconClass": "icon-calendar", "link": "/user/recent-booking"},
    "Settings": {"iconClass": "icon-settings", "link": "/user/settings"}
}

const navItems = Object.keys(navItemsMap).map(key =>
    <NavItem key={key} name={key} iconClass={navItemsMap[key]["iconClass"]} link={navItemsMap[key]["link"]}/>
)

const redirectToHomePage = () => {
    Router.push('/');
}

const SideBar = ({callback}: { callback: () => void }): React.ReactElement => {
    return (
        <nav id="sideBar" className="sidebar-wrapper">
            <div className="sidebar-content">
                <div className="sidebar-brand">
                    <a href="#">Frontlinecheck</a>
                    <div id="close-sidebar">
                        <FontAwesomeIcon icon={faTimes} className="fa-times"
                                         onClick={callback}/>
                    </div>
                </div>
                <div className="sidebar-menu">
                    <ul>
                        <li onClick={callback}>
                            <Link href={"/admin/home"}>
                                Home
                            </Link>
                        </li>
                        <li onClick={callback}>
                            <Link href={"/admin/websites"}>
                                Websites
                            </Link>
                        </li>
                        <li onClick={callback}>
                            <Link href={"/admin/keywords"}>
                                Keywords
                            </Link>
                        </li>

                        <li onClick={callback}>
                            <Link href={"/admin/advertisement"}>
                                Advertisement
                            </Link>
                        </li>
                        <li onClick={callback}>
                            <Link href={"/admin/profile"}>
                                Profile
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>

        </nav>
    )
};

export default SideBar;
