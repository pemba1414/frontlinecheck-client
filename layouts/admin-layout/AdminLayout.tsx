import React, { useEffect } from 'react';
import Sidebar from "./Sidebar";
import Logout from '../logout'
import {FiMenu} from 'react-icons/fi';
import { useState } from 'react';

export const AdminLayout = ({children}: { children: React.ReactElement }): React.ReactElement => {

    const [toggler, setToggler] = useState(true)

    return (

        <div className={`page-wrapper chiller-theme ${(toggler) ? '' : 'toggled' }`}>
        {/*<div className="page-wrapper chiller-theme toggled">*/}
            <a id="show-sidebar" className="btn btn-sm btn-dark" onClick={() => setToggler(false)}>
                <i className="fa fa-bars">
                    <FiMenu className="toggle-nav-menu" size={30}/>
                </i>
            </a>
            <Sidebar callback={() => setToggler(!toggler)}/>
                <Logout/>
            <main className="page-content">
                <header className="cf">
                    {children}
                </header>
            </main>
        </div>

    )
};






