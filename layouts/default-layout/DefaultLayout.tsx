import React from 'react';

export const DefaultLayout = ({ children }: { children: React.ReactElement }): React.ReactElement => (
    <div id="wrapper">
        <div id="content-wrapper" className="d-flex flex-column">
            <div id="content">
                {children}
            </div>
        </div>
    </div>
);
