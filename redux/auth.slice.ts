import { AnyAction } from 'redux';
import { HYDRATE } from 'next-redux-wrapper';
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import Router from "next/router";
import { removeCookie, setCookie } from "../utils/cookie";
import { AppState } from "./store";
import { UserDetails } from '../types/userDetails.type';

export interface AuthenticationState {
  token: string,
  role: string,
  secret?: string
}

const initialState: AuthenticationState = {
  token: null,
  role: null,
  secret: null
};

const setUserDetails = (state, token, role, secret) => {
  state.token = token
  state.role = role
  state.secret = secret
  setCookie('token', token);
  setCookie('role', role);
  setCookie('secret', secret);
}

export const slice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    authenticate: (state, action: PayloadAction<UserDetails>): void => {
      setUserDetails(state, action.payload.token, action.payload.role, action.payload.secret)
    },
    deAuthenticate: (state): void => {
      removeCookie('token');
      removeCookie('role');
      removeCookie('secret');
      Router.push('/authorized/login');
      state.token = null
      state.role = null
      state.secret = null
    },
    reAuthenticate: (state, action: PayloadAction<UserDetails>): void => {
      setUserDetails(state, action.payload.token, action.payload.role, action.payload.secret)
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(HYDRATE, (state, action: AnyAction) => {
        state.token = action.payload.auth.token
        state.role = action.payload.auth.role
        state.secret = action.payload.auth.secret
      })
  }
})

export const {
  authenticate,
  deAuthenticate,
  reAuthenticate
} = slice.actions

export const isLoggedIn = (state: AppState): boolean => {
  return state.auth.token !== null && state.auth.token != undefined
}

export const getToken = (state: AppState): string => state.auth.token

export const getRole = (state: AppState): string => state.auth.role

export const getSecret = (state: AppState): string => state.auth.secret

export default slice.reducer

