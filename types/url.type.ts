export type Site = {
    id: number,
    name: string,
    url: string
}