export type UserDetails = {
    token: string,
    role: string,
    secret: string | null
}