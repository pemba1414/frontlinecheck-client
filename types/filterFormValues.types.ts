export type FilterFormType = {
    website: string,
    keyword: string,
    startDate: string,
    endDate: string,
    status: string,
    featured: boolean
}