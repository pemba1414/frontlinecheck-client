import React from 'react';
import { useState } from 'react';
import { Button, Col, Row } from 'reactstrap';
import { News } from '../public-news/news/news.type';
import { StatusEnum } from '../public-news/news/StatusEnum';
import SocialShare from '../social-share';
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import parse from 'html-react-parser';
import Image from 'next/image'

const ModalBox = ({ actionTitle, title, createdDate, approvalStatus, hash, link, comments, publicUrlSlug, userUrlSlug, callback }
    : {
        actionTitle?: string
        title: string,
        createdDate: string,
        approvalStatus: string,
        hash: string,
        link?: string,
        comments?: string,
        publicUrlSlug: string,
        userUrlSlug: string,
        callback?: () => void
    }): React.ReactElement<News> => {
    return (
        <div className="modal-mask">
            {(approvalStatus == StatusEnum.APPROVED_WITH_NOTE) ?
                <div className="modal-content">
                    <div className="close-modal-btn">
                        <button className="dismiss-btn" onClick={callback}>
                            <span>×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="container-fluid">
                            <Row>
                                <Col md={4}>
                                    <div className="modal-brief">
                                        <div className="inner-card">
                                            <Image
                                                height={20}
                                                width={20}
                                                object-position="right bottom"
                                                layout="responsive"
                                                loading="lazy"
                                                quality="100"
                                                src={`${(hash === null) ?
                                                    '/images/no-image.svg'
                                                    :
                                                    process.env.NEXT_PUBLIC_API_HOST + 'api/file/view/' + hash
                                                    }
                                                        `}
                                            />
                                            <div className="news-title-modal">
                                                <a href={link} target="_blank"><h3>{title}</h3></a>
                                            </div>
                                            <div className="source-web modal-source">
                                                <img src="images/news-single.jpg" className="img-fluid" />
                                            </div>
                                            <div className="date-stamp-modal">
                                                <h6><FontAwesomeIcon icon={faCalendarAlt} />
                                                    {` ${createdDate}`}</h6>
                                            </div>
                                            <div className="modal-tag">
                                                <div className='comm-tag'>
                                                    <h5>Commentary</h5>
                                                </div>
                                                <div className="modal-socialmedia">
                                                    <SocialShare
                                                        url={`${process.env.NEXT_PUBLIC_API_HOST}${userUrlSlug}?uri=${publicUrlSlug}`}
                                                        title={actionTitle !== null ? actionTitle : title}
                                                        size={36}
                                                        rounded={true}
                                                        shareText='शेयर' />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={8}>
                                    <div className="modal-news-full">
                                        <h2>{(actionTitle) ? parse(actionTitle) : ''}</h2>
                                        {(comments) ? parse(comments) : ''}
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
                :
                <div className="modal-content valid-content">
                    <div className="close-modal-btn">
                        <button className="dismiss-btn" onClick={callback}>
                            {(approvalStatus === StatusEnum.DECLINED) ?
                                <span style={{ background: "#ED3E15" }}>×</span>
                                :
                                <span style={{ background: "#2CC441" }}>×</span>
                            }
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="container-fluid">
                            <div className="valid-brief">
                                <div className="inner-card">
                                    <div className="modal-pop">
                                        <Image
                                            height={140}
                                            width={260}
                                            layout="responsive"
                                            loading="lazy"
                                            quality="100"
                                            src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${hash}`}
                                        />
                                        <a href={link} target="_blank">
                                            <div className="news-title-modal">
                                                <h3>{title}</h3>
                                            </div>
                                        </a>
                                        <div className="source-web modal-source-valid">
                                            <img src="images/news-single.jpg" className="img-fluid" />
                                        </div>
                                        <div className="date-stamp-modal">
                                            <h6><FontAwesomeIcon icon={faCalendarAlt} />
                                                {` ${createdDate}`}</h6>
                                        </div>
                                        <div className="modal-tag">
                                            <div className=
                                                {`${approvalStatus == StatusEnum.DECLINED ?
                                                    'invalid-'
                                                    :
                                                    ''
                                                    }tag`}>
                                                <h5>{(approvalStatus == StatusEnum.DECLINED) ? "Invalid" : "Valid"}</h5>
                                            </div>
                                            <div className="modal-socialmedia">
                                                <SocialShare
                                                    url={`${process.env.NEXT_PUBLIC_API_HOST}${userUrlSlug}?uri=${publicUrlSlug}`}
                                                    title={title}
                                                    size={36}
                                                    rounded={true}
                                                    shareText='शेयर' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
        </div>
    )
}

export default ModalBox


