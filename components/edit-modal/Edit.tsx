import React from 'react';
import { Button, Input, Row, Form,  InputGroup, InputGroupText } from 'reactstrap';
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKeyboard, faGlobe } from '@fortawesome/free-solid-svg-icons';

const EditModal = (props) => {

    const [name, setName] = useState(props.name);
    const [url, setUrl] = useState(props.url);

    const handleName = (e) => {
        setName(e.target.value)
    }

    const handleUrl = (e) => {
        setUrl(e.target.value)
    }

    const formSubmit = (event) => {
        event.preventDefault()
        props.onHide()
        props.editWebsite(props.webId, name, url)
    }

    return (
        <div className="modal-mask">
            <div role="dialog" className="modal-container">
                <section id="modalDescription" className="modal-body">
                    <Row>
                        <Form onSubmit={formSubmit}>
                            <div className="row">
                            <div className="col-md-12">
                            <div className="wrap-input100  m-b-16 ">
							<Input type="text" name="name" value={name} placeholder="Name"
                                                onChange={handleName} className="input100 login-input" />
							<span className="focus-input100"></span>
							<span className="symbol-input100">
								<span className="lnr lnr-envelope">
                                <FontAwesomeIcon icon={faKeyboard} />
								</span>
							</span>
						</div>
                        </div>

                        <div className="col-md-12">
                        <div className="wrap-input100  m-b-16 ">
                        <Input type="text" name="url" defaultValue={props.url} placeholder="Url"
                                            onChange={handleUrl} className="input100 login-input" />
							<span className="focus-input100"></span>
							<span className="symbol-input100">
								<span className="lnr lnr-globe">
                                <FontAwesomeIcon icon={faGlobe} />
								</span>
							</span>
						</div>
                        </div>
                                {/* <div className="col-md-6">
                                    <div className="wrap-input100  m-b-16">
                                        <InputGroup className="input-group1">
                                            Name:
                                            <InputGroupText>
                                                <FontAwesomeIcon icon={faKeyboard} />
                                            </InputGroupText>
                                            <Input type="text" name="name" value={name} placeholder="Name"
                                                onChange={handleName} className="input100" />
                                        </InputGroup>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="wrap-input100  m-b-16">
                                    <InputGroup className="input-group2">
                                        URL:
                                            <InputGroupText>
                                                <FontAwesomeIcon icon={faGlobe} />
                                            </InputGroupText>
                                         <Input type="text" name="url" defaultValue={props.url} placeholder="Url"
                                            onChange={handleUrl} className="input100" />
                                            </InputGroup>
                                    </div>
                                </div> */}
                                <div className="row btnRow">
                                    <div className="col-md-6">
                                    <Button color="primary" type="submit" >Update</Button>
                                    </div>
                                    <div className="col-md-6">
                                    <Button color="secondary"  onClick={props.onHide}>Cancel</Button>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    </Row>
                </section>
            </div>
        </div>
    )
}
export default EditModal
