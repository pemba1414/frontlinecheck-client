import React from 'react'
import {SocialTypes} from './social.types'
import {FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon} from "react-share";
import { FaFacebookF, FaTwitter, FaTwitterSquare } from 'react-icons/fa';
import { RiFacebookBoxFill} from 'react-icons/ri';


const SocialShare = (props: SocialTypes): React.ReactElement<SocialTypes> => {
    return (
        <>
            <span className={(props.plainIcon) ? 'nav-socialBtn' : 'socialBtn'}>
            <FacebookShareButton
                url={props.url}
                quote={props.title}
            >
                {(props.plainIcon) ?
                    <RiFacebookBoxFill color="#3b5998" size={32}/>
                    :
                    <FaFacebookF/>
                }
                <span className="share-text">{props.shareText}</span>
            </FacebookShareButton>
            </span>
            <span className={(props.plainIcon) ? 'nav-socialBtn' : 'socialBtn'}>
            <TwitterShareButton
                url={props.url}
                via={props.title}
            >
                {(props.plainIcon) ?
                   <FaTwitterSquare color="#00aced" size={30}/>
                    :
                    <FaTwitter/>
                }
                <span className="share-text">{props.shareText}</span>
            </TwitterShareButton>
            </span>
        </>
    )
}
export default SocialShare

