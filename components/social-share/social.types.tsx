export type SocialTypes = {
    shareText?: string,
    size?: number,
    rounded?: boolean,
    url?: string,
    title?: string
    plainIcon?: boolean
}
