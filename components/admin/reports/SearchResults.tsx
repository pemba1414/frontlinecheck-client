import { useState } from 'react'
import {Spinner} from 'reactstrap'
const SearchResults = () => {
    const [loadingSpinner, setLoadingSpinner] = useState<boolean>(false)

    const websiteDetails = [
        {
            email: 'Laxman',
            results: 2,
            approvedResults:'giri',
            fetchedResults: 'some',
            resultFetched: 'hi'
        }
    ]

    return(
        <div>
            <h1>Total Number Of Users: 100</h1>
            <table className="m-t-20">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Number Of Results</th>
                                <th>Number of Approved Results</th>
                                <th>Last Fetched Result</th>
                                <th>Last Result Fetched On</th>
                            </tr>
                        </thead>
                        {
                            !loadingSpinner ?
                                <>
                                    <tbody>
                                        {websiteDetails.map((data, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{data.email}</td>
                                                    <td>{data.results}</td>
                                                    <td>{data.approvedResults}</td>
                                                    <td>{data.fetchedResults}</td>
                                                    <td>{data.resultFetched}</td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </>
                                : <>
                                    <div className="loader">
                                        <Spinner className="spinner" style={{ width: '5rem', height: '5rem' }}
                                            color="#2c3e50" />
                                    </div>
                                </>
                        }
                    </table>
        </div>
    )

}

export default SearchResults;