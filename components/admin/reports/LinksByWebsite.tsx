import React, { useState } from 'react'
import { Form, Input, Spinner, Row, Col, Button } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import * as AuthenticationSlice from "../../../redux/auth.slice";
import { useSelector } from 'react-redux';
import axios from 'axios'
import Link from 'next/link';


const LinksByWebsite = () => {

    const token: string = useSelector(AuthenticationSlice.getToken)
    const [loadingSpinner, setLoadingSpinner] = useState(false)
    const [websiteDetails, setWebsiteDetails] = useState([])
    const [text, setText] = useState('')

    const handleChange = (e) => {
        setText(e.target.value)
    }
    
    const submitData = (e): void => {
        e.preventDefault();
        setLoadingSpinner(true)
        axios.get(`/api/v2/website/list`,  {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                console.log(response)
                setWebsiteDetails(response.data.websites)
                setLoadingSpinner(false)
            })
            .catch(() => setLoadingSpinner(false))
    }

    return (
        <div>
            <div className="wrap-input100  m-b-16 email-search">
                <h3>Find Website By Name Or URL Like</h3>
                <Form onSubmit={submitData}>
                    <Row>
                        <Col md={4}>
                            <Input type="text" className="input100 email-search" placeholder="Website" onChange={handleChange} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100 search-symbol">
                                <span className="lnr lnr-emvelope">
                                    <FontAwesomeIcon icon={faEnvelope} />
                                </span>
                            </span>
                        </Col>
                        <Col md={3}>
                            <button type="submit" className="btn btn-primary">Search</button>
                        </Col>
                    </Row>
                </Form>
            </div>
            <Col md={9}>
                <div className="email-table">
                    <table className="m-t-20">
                        <thead>
                            <tr>
                                <th>Website</th>
                                <th>Created Count</th>
                                <th>Scrapped Count</th>
                                <th>Failed Count</th>
                                <th>CreatedToday Count</th>
                                <th>ScrappedToday Count</th>
                                <th>FailedToday Count</th>
                                <th>Last Created Date</th>
                                <th>Last Scrapped Date</th>
                            </tr>
                        </thead>
                        {
                            !loadingSpinner ?
                                <>
                                    <tbody>
                                        {websiteDetails.map((data, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{data.id}</td>
                                                    <td>
                                                    <Link href="/admin/edit-website/[id]" as={`/admin/edit-website/${data.id}`}>
                                                        {data.name}
                                                    </Link>
                                                        </td>
                                                    <td>{data.url}</td>
                                                    <td>{data.disabledDate}</td>
                                                    <td>
                                                       <Button>
                                                       <Link href="/admin/edit-website/[id]" as={`/admin/edit-website/${data.id}`}>
                                                           Edit
                                                        </Link>
                                                           </Button> 
                                                        <Button>Delete</Button>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </>
                                : <>
                                    <div className="loader">
                                        <Spinner className="spinner" style={{ width: '5rem', height: '5rem' }}
                                            color="#2c3e50" />
                                    </div>
                                </>
                        }
                    </table>
                </div>
            </Col>

        </div>
    )
}

export default LinksByWebsite