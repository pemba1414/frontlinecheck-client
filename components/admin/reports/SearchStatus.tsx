const SearchStatus = () => {
    return(
        <div>
            <table>
                <tbody>
                    <tr>
                        <th>Total Search Results</th>
                        <td>2000</td>
                    </tr>
                    <tr>
                        <th>Search Results Yesterday</th>
                        <td>100</td>
                    </tr>
                    <tr>
                        <th>Total Links</th>
                        <td>230</td>
                    </tr>
                    <tr>
                        <th>Active Links</th>
                        <td>21</td>
                    </tr>
                    <tr>
                        <th>Scrapped Links</th>
                        <td>12</td>
                    </tr>
                    <tr>
                        <th>Scraping Failed Links</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Expired Links</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links To Be Scraped</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Old Links To Be Scrapped</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links To Be Scraped Today</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Added Today</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Scrapped Today</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Failed Today</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Expired Today</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Added Yesterday</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Scrapped Yesterday</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links  Failed Yesterday</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Links Expired Yesterday</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Last Link Added On</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Last Link  Scrapped On</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Last Link Failed On</th>
                        <td>10</td>
                    </tr>
                    <tr>
                        <th>Last Link Expired On</th>
                        <td>10</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default SearchStatus