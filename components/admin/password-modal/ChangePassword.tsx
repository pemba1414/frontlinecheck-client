import axios from 'axios';
import React, { useState } from 'react';
import { Button, Col, Input, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import * as AuthenticationSlice from "../../../redux/auth.slice";
import { useSelector } from "react-redux";

type ChangePasswordProps = {
    name: string,
    email: string,
    closeModal: () => void
}


const ChangePassword = (props: ChangePasswordProps) => {
    const [password, setPassword] = useState<string>('')
    const token: string = useSelector(AuthenticationSlice.getToken)

    const handlePassword = (e): void => {
        setPassword(e.target.value)
    }

    const btnChangePassword = (): void => {
        axios.post('/api/v1/user/changePasswordByAdmin', {
            name: props.name,
            email: props.email,
            password: password
        }, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => console.log(response))
            .catch((error) => console.error(error))
        props.closeModal()
    }

    return (
        <div className="modal-mask">
            <div role="dialog" className="modal-container">
                <h3>Change Password</h3>
                <header id="modalTitle" className="modal-header">
                    <Button type="button" aria-label="Close modal" className="btn-close" onClick={() => props.closeModal()}>X</Button>
                </header>
                <section id="modalDescription" className="modal-body">
                    <Row>
                        <Col md={12}>
                            <Row>
                                Name
                                <Input type="text" value={props.name} />
                            </Row>
                            <Row>
                                Email
                                <Input type="email" value={props.email} />
                            </Row>
                            <Row>
                                Password
                                <Input type="password" onChange={handlePassword} />
                            </Row>
                            <Button type="submit" onClick={btnChangePassword}>Submit</Button>
                        </Col>
                    </Row>
                </section>
            </div>
        </div>
    )
}
export default ChangePassword;
