import React, { useState } from 'react'
import { Form, Input, Spinner, Row, Col } from 'reactstrap'
import axios from 'axios'
import * as AuthenticationSlice from "../../../redux/auth.slice";
import { useDispatch, useSelector } from 'react-redux';
import ChangePassword from '../password-modal/ChangePassword'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKeyboard, faLink, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import Router from 'next/router'
import Table from '../table-data/Table'


type UsersInfo = {
    name: string,
    email: string,
    urlSlug: string,
    id: number
}

const UserInfoTable = () => {
    const dispatch = useDispatch()
    const [searchItem, setSearchItem] = useState<string>('')
    const [userDetail, setUserDetail] = useState<UsersInfo[]>([])
    const [name, setName] = useState<string>('')
    const [email, setEmail] = useState<string>('')
    const [openModal, setOpenModal] = useState<boolean>(false)
    const [updateEmail, setUpdateEmail] = useState<string>(null)
    const [updateUrlSlug, setUpdateUrlSlug] = useState<string>(null)
    const [changeFeatured, setChangeFeatured] = useState<boolean>(true)
    const token: string = useSelector(AuthenticationSlice.getToken)
    const [enableUser, setEnableUser] = useState<boolean>(true)
    const [loadingSpinner, setLoadingSpinner] = useState<boolean>(false)
    const [userEmail, setUserEmail] = useState<string>('')
    const [isFeatured, setIsFeatured] = useState<boolean>(true)

    const handleChange = (e): void => {
        setSearchItem(e.target.value)
    }

    const submitData = (e): void => {
        e.preventDefault();
        setLoadingSpinner(true)
        axios.get(`/api/v2/user?emailSlug=${searchItem}`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                setUserDetail(response.data)
                setLoadingSpinner(false)
            })
            .catch(() => setLoadingSpinner(false))
    }

    const btnChangePassword = (name, email): void => {
        setName(name)
        setEmail(email)
        setOpenModal(true)
    }

    const closeModal = (): void => {
        setOpenModal(false)
    }

   
   
    const updateForm = (event): void => {
        event.preventDefault()
        setLoadingSpinner(true)
        axios.post('/api/v2/user/updatePublicPage', {
            email: updateEmail,
            url: updateUrlSlug
        }, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response) {
                setLoadingSpinner(false)
            }
        }).catch(() => setLoadingSpinner(false))
    }

    const loginUser = (e): void => {
        e.preventDefault();
        axios.post('/api/auth/loginAsUser', {
            email: userEmail
        }, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            const token = response.data.accessToken
            const role = response.data.authorities[0].authority
            dispatch(AuthenticationSlice.authenticate({ token: token, role: role, secret: response.data.secret }))
            Router.push('/admin/home')
        })
            .catch((error) => console.error(error))
    }

    return (
        <div>
            <div className="wrap-input100  m-b-16 email-search">
                <h4>Find User By Email:</h4>
                <Form onSubmit={submitData}>
                    <Row>
                        <Col md={4}>
                            <Input type="text" className="input100 email-search" placeholder="Email" onChange={handleChange} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100 search-symbol">
                                <span className="lnr lnr-emvelope">
                                    <FontAwesomeIcon icon={faEnvelope} />
                                </span>
                            </span>
                        </Col>
                        <Col md={3}>
                            <button type="submit" className="btn btn-primary">Search</button>
                        </Col>
                    </Row>
                </Form>
            </div>
            <Col md={9}>
                <div className="email-table">
                    <table className="m-t-20">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Url Slug</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        {
                            !loadingSpinner ?
                                <>
                                    <tbody>
                                        {userDetail.map((data, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{data.email}</td>
                                                    <td>{data.name}</td>
                                                    <td>{data.urlSlug}</td>

                                                    <td>
                                                       <Table  id={data.id}/>
                                                        <br />
                                                        <a className="btn btn-primary form-control" css="color: white; cursor:pointer" onClick={() => btnChangePassword(data.name, data.email)}>Change Password</a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </>
                                : <>
                                    <div className="loader">
                                        <Spinner className="spinner" style={{ width: '5rem', height: '5rem' }}
                                            color="#2c3e50" />
                                    </div>
                                </>
                        }
                    </table>
                </div>
            </Col>

            <h4>Update Public Page</h4>

            <div className="wrap-input100  m-b-16 email-search update-public">
                <Form onSubmit={loginUser}>
                    <Row>
                        <Col md={4}>
                            <Input type="text" className="input100 email-search" placeholder="Email" onChange={(e) => setUpdateEmail(e.target.value)} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100 search-symbol">
                                <span className="lnr lnr-envelope">
                                    <FontAwesomeIcon icon={faEnvelope} />
                                </span>
                            </span>
                        </Col>
                        <Col md={4}>
                            <Input type="text" className="input100 email-search" placeholder="Url Slug" onChange={(e) => setUpdateUrlSlug(e.target.value)} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100 search-symbol">
                                <span className="lnr lnr-link">
                                    <FontAwesomeIcon icon={faLink} />
                                </span>
                            </span>
                        </Col>
                        <Col md={3}>
                            <button type="submit" className="btn btn-primary">Update</button>
                        </Col>
                    </Row>
                </Form>
            </div>
        
            <h4>Login As User</h4>
            <div className="wrap-input100  m-b-16 email-search">
                <Form onSubmit={loginUser}>
                    <Row>
                        <Col md={4}>
                            <Input type="text" className="input100 email-search" placeholder="Email" onChange={(e) => setUserEmail(e.target.value)} />
                            <span className="focus-input100"></span>
                            <span className="symbol-input100 search-symbol">
                                <span className="lnr lnr-envelope">
                                    <FontAwesomeIcon icon={faEnvelope} />
                                </span>
                            </span>
                        </Col>
                        <Col md={3}>
                            <button type="submit" className="btn btn-primary">Login As User</button>
                        </Col>
                    </Row>
                </Form>
            </div>

            {
                openModal ?
                    <ChangePassword closeModal={closeModal} name={name} email={email} />
                    : ''
            }
        </div>
    )
}
export default UserInfoTable