import React from 'react';
import { NavLink } from "reactstrap";

type Props = {
    link: string,
    iconClass: string,
    name: string
}
const NavItem = (props: Props): React.ReactElement => (
    <li className="nav-item">
        <NavLink to={props.link}>
            <i className={props.iconClass} />
        &nbsp;
        <span>{props.name}</span>
        </NavLink>
    </li>
);

export default NavItem;
