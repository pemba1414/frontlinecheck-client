import axios from 'axios';
import React, { useEffect } from 'react'
import { useState } from 'react';
import { Row, Form, Dropdown, FormGroup, Label, Input, Button, Col } from 'reactstrap'
import {useSelector} from "react-redux";
import {Spinner} from 'reactstrap';
import * as AuthenticationSlice from "../../../redux/auth.slice";
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'


 export type FilterFormType = {
    websiteId: number,
    keywordIdentifier: string,
    dateRange: string,
    approvalStatus: string,
    featured: boolean
}

type website = {
    id: number,
    name: string
}

type keyworddetails = {
    id: string,
    name: string
}

const FilterForm = ({callback}) => {
    const token = useSelector(AuthenticationSlice.getToken)
    const [loadingSpinner, setLoadingSpinner] = useState<boolean>(true)
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [approvalStatus, setApprovalStatus] = useState<string>(null);
    const [selectedWebsites, setSelectedWebsites] = useState<number>();
    const [selectedKeywords, setSelectedKeywords] = useState<string>(null);
    const [websites, setWebsites] = useState<website[]>(null)
    const [keywords, setKeywords] = useState<keyworddetails[]>([])
    const [state, setState] = useState({
        startDate: null,
        endDate: null
      });
    const [featuredValue, setFeaturedValue] = useState<boolean>(null);
    const [focusedInput, setFocusedInput] = useState<boolean>(null);
   
    let dateRange: string

    
    let startdate = moment(state.startDate).format("DD/MM/YYYY");
    let enddate = moment(state.endDate).format("DD/MM/YYYY")

    if(state.startDate === null && state.endDate === null){
         dateRange = null
    }else{
         dateRange = startdate+'-'+enddate
    }

    const toggle = () => setDropdownOpen(!dropdownOpen);

    const fetchWebsites = (): void => {
        let List: website[] = []
        axios.get(`/api/v2/userWebsite/list`, {headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }})
        .then((response) => {
            response.data.userWebsites.map((value: website, index) => {
                List.push({id: value.id, name: value.name})
            })
            setWebsites(List)
        })
        .catch((error) => console.error(error))
    }

    let List: keyworddetails[] = []

    const fetchKeywords = (): void => {
        axios.get(`/api/v2/keyword/list`, {headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }})
        .then((response) => {
            response.data.map((value: keyworddetails, index) => {
                List.push({id: 'K'+'_'+value.id, name: value.name})
            })
            
        })
        .catch((error) => console.error(error))
    }

    const fetchGroupKeyword = (): void => {
        axios.get('/api/v2/keywordGroup/list', {headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }})
        .then((response) => {
            response.data.map((value: keyworddetails, index) => 
            List.push({id: 'KG'+'_'+value.id, name: value.name}) 
            )
            setKeywords(List)
        })
        .catch((error) => console.error(error))
    }

    useEffect(() => {
        fetchWebsites(), fetchKeywords(), fetchGroupKeyword()
    },[])
    
    const approvalList = ['Pending',
        'Approved',
        'Declined',
        'Approved with note'];

    const handleWebsite = (e) => {
        if(e.target.value){
            setSelectedWebsites(e.target.value)
        }
    }

    const handleKeywordIdentifier = (e) => {
        if(e.target.value){
        setSelectedKeywords(e.target.value)
        }
    }

    const handleApprovalList = (e) => {
        if(e.target.value){
        setApprovalStatus(e.target.value)
        }
    }

   

    const handleChange = (e) => {
        if(e.target.checked){
            setFeaturedValue(true)
        }else{
            setFeaturedValue(false)
        }  
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        callback(selectedValues)
    }

    let selectedValues: FilterFormType = {
        websiteId: Number(selectedWebsites),
        keywordIdentifier: selectedKeywords,
        approvalStatus: approvalStatus,
        dateRange: dateRange,
        featured: featuredValue
    };

   
    return (
        <Form onSubmit={handleFormSubmit}>
            <Col lg={12} md={12}>
            <Row className={"margin-reset filter-warp"}>
            <Col md={2}>
            <FormGroup>
                {/* <Label for="select">
                        Website
                </Label> */}
                <Input type="select" name="select" id="exampleSelect" className="form-control" onChange={handleWebsite}>
                                <option>Select Website</option>
                                {
                                    websites && websites.map((val, index) => 
                                       <option value={val.id} key={index}>{val.name}</option>
                                       
                                    )}
                </Input>
                </FormGroup>
            </Col>
                <Col md={2}>
                {/* <Label for="select">
                        Keyword
                </Label> */}
                <Input type="select" name="select" id="exampleSelect" onChange={handleKeywordIdentifier}>
                                <option>Select Keyword</option>
                                {keywords && keywords.map((keyword, index) => {
                                                return (
                                                    <option key={index} value={keyword.id}>{keyword.name}</option>
                                                )
                                            })}
                </Input>
                </Col>
                <Col md={4}>
                    {/* <label>Date</label> */}
                    <DateRangePicker
                   isOutsideRange={() => false}
                     startDate={state.startDate} 
                     endDate={state.endDate} 
                     onDatesChange={({ startDate, endDate }) => setState({ startDate, endDate })}
                     focusedInput={focusedInput} 
                     onFocusChange={focusedInput => setFocusedInput(focusedInput)}

                    />


                </Col>
                <Col md={2} className='approval-status'>
                {/* <Label for="select">
                        Approval Status
                </Label>  */}
               
                <Input type="select" name="select" id="exampleSelect" onChange={handleApprovalList}>
                                <option>Select Approval Status</option>
                                {approvalList && approvalList.map((list, index) => {
                                                return (
                                                    <option key={index} value={list}>{list}</option>
                                                )
                                            })}
                            </Input>
                </Col>

                <Col md={1}>
                    <div className="form-check">
                        <Input type="checkbox" onChange={handleChange}/>
                        <label className="form-check-label">Featured</label>
                    </div>
                    {/* <label className="form-check-label">Featured</label> */}
                </Col>
                <Col md={1}>
                   <div className="filter-btn">
                   <Button color="primary" type="submit">Filter</Button>
                   </div>
                </Col>
            </Row>
            </Col>
        </Form>
    )
}

export default FilterForm