import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

type ListElement = {
    id: number,
    name: string,
    listName: string,
    callback?: (id: number, name: string, listname: string) => void
}
const ListElement = (props: ListElement): React.ReactElement<ListElement> => {

    const [showBtn, setShowBtn] = useState(false);

    return (
        <span
            onMouseEnter={() => setShowBtn(true)}
            onMouseLeave={() => setShowBtn(false)}>
            <li key={props.id} className="animated">
                {props.name}
                <a className="delete-button" onClick={(e) => props.callback(props.id, props.name, props.listName)}>
                    <FontAwesomeIcon className={`${(showBtn) ? '' : 'hidden'}`} icon={faTimesCircle} size="1x"
                    /></a>
            </li>
        </span>
    )
}
export default ListElement
