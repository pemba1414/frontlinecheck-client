import React from 'react';
import {useEffect} from 'react';
import {Col, Row} from 'reactstrap';
import {KeywordType} from '../../../types/keyword.type';
import ListElement from '../list-element';
import {BeatLoader} from 'react-spinners';

type KeywordProps = {
    list: KeywordType[],
    listName: string,
    callback: (id: number, name: string, listname: string) => void
}
const KeywordsList = (props: KeywordProps): React.ReactElement<KeywordProps> => {

    return (
        <Row>
            <Col>
                <ul className="keywords-list">
                    {(props.list != undefined) ?
                        props.list.map((keyword, index) => {
                            return (
                                <ListElement
                                    key={`keyword-${index}`}
                                    name={keyword.name}
                                    id={keyword.id}
                                    listName={props.listName}
                                    callback={props.callback}
                                />
                            )
                        })
                        :
                        <span className="loader"><BeatLoader/></span>
                    }
                </ul>
            </Col>
        </Row>
    )
}
export default KeywordsList