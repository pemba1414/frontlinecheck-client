import BannerList from './BannerList'
const Banner = (props) => {
    return(
        <div>
             <ul className="keywords-list">
                {props.banners && props.banners.map((banner, index) => {
                    return <BannerList  key={index} items={banner}  />
                })}
            </ul>
        </div>
    ) 
}

export default Banner;
