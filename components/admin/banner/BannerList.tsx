import Image from 'next/image'

const BannerList = (props) => {
    return(
       <div>
           <Image
                    height={10}
                    width={10}
                    layout="responsive"
                    loading="lazy"
                    src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${props.hash}`}
                    alt="image here"
            />
        </div>
    )
}

export default BannerList