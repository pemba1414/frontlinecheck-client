
import { Form, Input, Spinner } from 'reactstrap'
import { useState } from 'react'
import axios from 'axios'
import * as AuthenticationSlice from "../../../redux/auth.slice";
import { useSelector } from 'react-redux';

const Table = ({ id }) => {

    const token: string = useSelector(AuthenticationSlice.getToken)
    const [changeFeatured, setChangeFeatured] = useState<boolean>(true)
    const [enableUser, setEnableUser] = useState<boolean>(true)

    const makeuserfeatured = (event, id: number): void => {
        event.preventDefault()
        axios.get(`/api/v2/user/${id}/makeFeatured`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response) {
                setChangeFeatured(!changeFeatured)
        
            }
        }).catch((error) => console.error(error))
    }

    const removeuserfeatured = (event, id): void => {
        event.preventDefault()
        axios.get(`/api/v2/user/${id}/removeFromFeatured`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response) {
                setChangeFeatured(!changeFeatured)
            }
        }).catch((error) => console.error(error))
    }

    const disableUser = (event, id): void => {
        event.preventDefault()
                axios.get(`/api/v2/user/${id}/disable`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response) {
                setEnableUser(!enableUser)
                
            }
        }).catch((error) => console.error(error))
    }

    const userEnable = (event, id): void => {
        event.preventDefault()
                axios.get(`/api/v2/user/${id}/enable`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response) {
                setEnableUser(!enableUser)
                
            }
        }).catch((error) => console.error(error))
    }


    return (
        <div>
            {
                    changeFeatured ?
                        <Form>
                            <Input type="submit" value="Make User Featured" className="btn btn-success form-control" onClick={() => makeuserfeatured(event, id)} />
                        </Form> :
                        <Form>
                            <Input type="submit" value="Remove From Featured" className="btn btn-danger form-control" onClick={() => removeuserfeatured(event, id)} />
                        </Form>

            }
            <br></br>
            {
                enableUser ?
                <Form>
                    <Input type="submit" value="Disable User" className="btn btn-danger form-control" onClick={() => disableUser(event, id)} />
                </Form> :
                <Form>
                <Input type="submit" value="Enable User" className="btn btn-success form-control" onClick={() => userEnable(event, id)} />
            </Form>
            }


        </div>
    )
}

export default Table