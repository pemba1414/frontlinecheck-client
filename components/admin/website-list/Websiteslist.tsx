
import Website from '../../../components/admin/website-list/Website'
import { Row, Col } from 'reactstrap'

const Websiteslist = ({ websitesList, deleteSite, editWebsite }) => {
    return (
        <Row>
            <Col>
                <ul className="keywords-list">
                    {websitesList.map((val, index) => (
                        <Website id={val.id} key={index} val={val} editWebsite={editWebsite} deleteSite={deleteSite} />
                    ))}
                </ul>
            </Col>
        </Row>
    )
}

export default Websiteslist;