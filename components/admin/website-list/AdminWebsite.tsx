import React, { useState, useEffect} from 'react'
import axios from 'axios';
import { Form, Input, Label, Button} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKeyboard, faGlobe } from '@fortawesome/free-solid-svg-icons';
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../../redux/auth.slice";
import Websiteslist from '../../../components/admin/website-list';
import ConfirmationModal from '../../../components/confirmation-modal/ConfirmationModal';

const AdminWebsites = (): React.ReactElement => {

  const token = useSelector(AuthenticationSlice.getToken)
  const [currentPage, setCurrentPage] = useState(0);
  const [checked, setChecked] = useState(false)
  const [openModal, setOpenModal] = useState(false);
  const [deleteKeyID, setDeleteKeyID] = useState()
  const [deleteKey, setDeleteKey] = useState(false)
  const [title, setTitle] = useState('');
  const [checkAll, setCheckAll] = useState(false);
  const [deleteMsg, setDeleteMsg] = useState('');
  const [loadingSpinner, setLoadingSpinner] = useState<boolean>(true);
  const [addSites, setAddSites] = useState([])
  const [sites, setSites] = useState([]);
  const [checkedValues, setCheckedValues] = useState<number[]>([]);
  const [website, setWebsite] = useState({
    name: "",
    url: ""
  });
  const [websiteList, setWebsiteList] = useState([]);

  const onChangePage = (data) => {
    setLoadingSpinner(true);
    setCurrentPage(data.selected);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setWebsite((prevData) => {
      return {
        ...prevData,
        [name]: value
      };
    });
  };

  const addOtherWebsites = (event) => {
    if (event.target.checked) {
      const num = Number(event.target.value)
      setCheckedValues([...checkedValues, num])
    } else {
      setCheckedValues(checkedValues.filter(x => x != event.target.value))
    }
  }

  const deleteSite = (id, name) => {
    setOpenModal(true)
    setTitle('Confirmation');
    setDeleteMsg('Are you sure you want to delete  ' + name + '?');
    setDeleteKeyID(id); //set in state and accessible anywhere
    setDeleteKey(true);
    setOpenModal(true)
  }

  const confirmDelete = (deleteKey: boolean) => {
    setOpenModal(false);
    (deleteKey) ?
      axios.delete(`/api/v2/userWebsite/${deleteKeyID}/delete`, {
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      })
        .then(response => {
          setSites(response.data.userWebsites)
          setAddSites(response.data.otherWebsites)
        })
        .catch(error => console.error)
      :
      ''
  }

  const checkboxFormSubmit = (e) => {
    e.preventDefault();
    axios.post(`/api/v2/userWebsite/createFromList`, {
      websiteIds: checkedValues
    }, {
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        setSites(response.data.userWebsites);
        setAddSites(response.data.otherWebsites)
      })
      .catch((error) => {
        console.error(error)
      })
    setCheckedValues([])
  }

  const formSubmit = (e) => {
    axios.post('/api/v2/userWebsite/create',
      website,
      {
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        setSites(response.data.userWebsites)
        setAddSites(response.data.otherWebsites)
      })
      .catch((error) => {
        console.error(error)
      })

    setWebsite({
      name: "",
      url: ""
    })
    e.preventDefault();
  };

  const editWebsite = (id, name, url) => {
    axios.put(`/api/v2/userWebsite/${id}/update`, {
      name, url
    }, {
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
    })
      .then((response) =>
        setSites(response.data.userWebsites)
      )
      .catch((error) => {
        console.log(error)
      })
  }

  const fetchApi = () => {
    axios.get(`/api/v2/userWebsite/list`, {
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        setSites(response.data.userWebsites)
        setAddSites(response.data.otherWebsites)
      })
      .catch((error) => {
        console.error(error)
      })
  }

  useEffect(() => {
    fetchApi()
  }, [])

  const selectAll = (event) => {
    let list = [];
    if (event.target.checked) {
      addSites.map((val, index) => {
        list.push(val.id)
      })
      setCheckedValues(list)
    } else {
      setCheckedValues([])
    }

  }

  return (
      <div className="website-warp">

          <h4 className="m-b-20">Your Websites</h4>
          {
            sites.length < 1 ? 'No websites available currrently' :
              <>
                <Websiteslist websitesList={sites} deleteSite={deleteSite} editWebsite={editWebsite} />
              </>
          }

            {
              addSites.length < 1 ? '' :
                <>
                <div className="available-website">
                  <h4>Other Websites Available</h4>
                  <Form onSubmit={checkboxFormSubmit} className="checkboxform">
                    <div className="selection-check">
                      <Input type="checkbox" onChange={selectAll} className='checkbox-label' />
                      <Label className="select">Select All</Label>
                    </div>
                    <div className="selection">
                      {
                        addSites && addSites.map((val, index) =>
                          <div key={index} className="selection-list">
                            <Input type="checkbox" className="checkbox-label" onChange={addOtherWebsites} value={val.id} checked={checkedValues.find(value => val.id === value) ? true : false} />
                            <Label className="checkBoxLabel"> {val.name} </Label>
                          </div>
                        )
                      }
                    </div>

                    <div className="web-btns">
                      <Button color="primary" type="submit">Add Selected</Button>
                    </div>

                  </Form>
                  </div>
                </>}
         

          <h4 className="m-b-20">Add New Website</h4>

          <Form onSubmit={formSubmit}>
            <div className="row">
              <div className="col-md-6">
                <div className="wrap-input100  m-b-16">
                  <Input type="text" name="name" value={website.name} placeholder="Name"
                    onChange={handleChange} required className="input100" />
                  <span className="focus-input100"></span>
                  <span className="symbol-input100">
                    <span className="lnr lnr-keyboard">
                      <FontAwesomeIcon icon={faKeyboard} />
                    </span>
                  </span>
                </div>
              </div>
              <div className="col-md-6">
                <div className="wrap-input100  m-b-16">
                  <Input type="text" name="url" value={website.url} placeholder="Url"
                    onChange={handleChange} required className="input100" />
                  <span className="focus-input100"></span>
                  <span className="symbol-input100">
                    <span className="lnr lnr-keyboard">
                      <FontAwesomeIcon icon={faGlobe} />
                    </span>
                  </span>
                </div>
              </div>
              <div className="add-web-btns">
                <Button color="primary" className="btnAdd" type="submit">Add</Button>
                <a href="/scrapper/" >
                  <Button color="secondary" className="btnAdd">Cancel</Button></a>
              </div>
            </div>
          </Form>

      <>{openModal ?
        <ConfirmationModal
          heading={title}
          description={deleteMsg}
          deleteKey={deleteKey}
          callback={confirmDelete}
        />
        :
        " "}</>
    
    </div>
  )
}

export default AdminWebsites;
