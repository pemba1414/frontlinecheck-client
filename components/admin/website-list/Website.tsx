import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import EditModal from '../../edit-modal/Edit';
import { useState } from 'react';

const Website = ({ id, val, deleteSite, editWebsite }) => {
    const [showBtn, setShowBtn] = useState(true);
    const [openModal, setOpenModal] = useState(false);
    const [name, setName] = useState('')
    const [url, setUrl] = useState('')
    const [webId, setWebId] = useState()
    const [website, setWebsite] = useState();

    const openEditModal = () => {
        setOpenModal(true)
        setWebsite(val)
        setName(val.name)
        setUrl(val.url)
        setWebId(id)
    }

    const toggle = () => {
        setOpenModal(false)
    }

    return (
        <span
            onMouseEnter={() => setShowBtn(true)}
            onMouseLeave={() => setShowBtn(false)}>
            <li className="animated">
                <div onClick={openEditModal} >
                    <FontAwesomeIcon icon={faLink} />
                    {val.name}
                </div>
                <a className="delete-button" onClick={() => deleteSite(id, val.name)}>
                    <FontAwesomeIcon className={`${(showBtn) ? '' : 'hidden'}`} icon={faTimesCircle} size="1x" />
                </a>
            </li>
            {
                openModal ? <EditModal onHide={toggle} webId={webId} modal={openModal} editWebsite={editWebsite} name={name} url={url} /> : ''
            }
        </span>
    )
}

export default Website;