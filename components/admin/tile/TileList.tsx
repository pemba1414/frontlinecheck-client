
import Image from 'next/image'
const TileList = (props) => {
    return(
       <div>
      <Image
                    height={100}
                    width={100}
                    layout="responsive"
                    loading="lazy"
                    src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${props.hash}`}
                    alt="image here"
                />
        </div>
    )
}

export default TileList