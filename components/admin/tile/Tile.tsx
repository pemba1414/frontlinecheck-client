import TileList from './TileList'
const Tile = (props) => {
    return (
        <div>
            <ul className="keywords-list">
                {props.tiles.map((tile, index) => {
                    return <TileList key="index" items={tile} />
                })}
            </ul>
        </div>
    )
}

export default Tile;
