import React from 'react'
import { Row } from 'reactstrap'
import NewsCard from '../../admin/news-card/index'
import {News} from "../../public-news/news/news.type";

type NewsGroupProps = {
    news: News[],
    callback: (id: number, approvalStatus: string) => void
}


const NewsGroup = (props: NewsGroupProps): React.ReactElement<News> => {
    return (
        <Row>
            {props.news.map((newsItem, index) => {
                return <NewsCard key={`newsItem-${index}`}
                                 title={newsItem.title}
                                 link={newsItem.link}
                                 callback={props.callback}
                                 id={newsItem.id}
                                 featured={newsItem.featured}
                                 actionTitle={newsItem.actionTitle}
                                 keywords={newsItem.keywords}
                                 createdDate={newsItem.createdDate}
                                 approvalStatus={newsItem.approvalStatus}
                                 hash={newsItem.hash}
                                 comments={newsItem.comments}
                />
            })}
        </Row>
    
    )

}

export default NewsGroup