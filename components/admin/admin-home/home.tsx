import React from 'react'
import { useState } from 'react'
import FilterForm from '../../admin/filter-form'
import NewsGroup from '../../admin/news-group'
import ReactPaginate from 'react-paginate'
import { Spinner } from 'reactstrap'
import { useEffect } from 'react'
import Link from 'next/link'
import { useSelector } from "react-redux";
import * as AuthenticationSlice from "../../../redux/auth.slice";
import axios from 'axios'
import { News } from '../../public-news/news/news.type'
import { Button } from 'reactstrap'

type list = {
  comments: string,
  featured: boolean,
  title: string,
  status: string,

}

const AdminHome = (): React.ReactElement => {
  const token: string = useSelector(AuthenticationSlice.getToken)
   
  const [totalPages, setTotalPages] = useState(1)
  const [newsCheck, setNewsCheck] = useState(false)
  const [newsLength, setNewsLength] = useState<number>()
  const [comments, setComments] = useState<string>('')
  const [title, setTitle] = useState<string>('')
  const [featured, setFeatured] = useState<boolean>(null)
  const [perPage, setPerPage] = useState<number>(0)
  const [currentPage, setCurrentPage] = useState<number>(0)
  const [loadingSpinner, setLoadingSpinner] = useState<boolean>(true)
  const [news, setNews] = useState<News[]>([])


  const fetchApi = (): void => {
    axios.post(`/api/v2/searchResult/latestResults?page=${currentPage}`, {},
      {
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        setNews(response.data.list)
        setPerPage(response.data.totalPages)
        setLoadingSpinner(false)
        setNewsLength(response.data.list.length)
      })
      .catch((error) => {
        console.error(error)
        setNewsCheck(false)
      })
  }

  useEffect(() => {
    fetchApi()
  }, [currentPage])


  const onChangePage = (data): void => {
    setCurrentPage(data.selected)
  }

  const handleFilterValues = (selectedValues): void => {
    setLoadingSpinner(true)
    axios.post(`/api/v2/searchResult/latestResults?page=${currentPage}`, selectedValues,
      {
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        setNews(response.data.list)
        setPerPage(response.data.totalPages)
        setLoadingSpinner(false)
        setNewsLength(response.data.list.length)
      })
      .catch((error) => {
        setLoadingSpinner(false)
        console.error(error)
      })
  }

  const loadSpinner = (id, approvalStatus): void => {
      setLoadingSpinner(true)
      axios.put(`/api/v2/searchResult/${id}/approvalStatus/update`,
      {comments, featured, approvalStatus, title},  {headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
      }})
      .then((response) => {
        const result = news.findIndex(news => news.id === response.data.id );
        news[result] = response.data
        setNews(news)
        setLoadingSpinner(false)
          })
      .catch((error) => console.error(error))
    }

    let pageRange: number = (newsLength <= 24) ? 1 : 3
  return (
    <div>
      <div className="vld-parent">
      {/* <h4>News Filter</h4> */}
            <FilterForm callback={handleFilterValues} />
            <div className="add-btns">
              <Button color="primary"><Link href={"/admin/add-news"}>Add News Manually</Link></Button>
              <br></br>
              <Button color="primary"className={"second-btn"}><Link href={"#"}>Fetch Latest Results</Link></Button>
            </div>
            { !loadingSpinner ?
          <>
            <NewsGroup  news={news}  callback={loadSpinner} />
            <div className="container">
                  <ReactPaginate
                    previousLabel={'PREV'}
                    nextLabel={'NEXT'}
                    breakLabel={'...'}
                    pageCount={perPage}
                    onPageChange={onChangePage}
                    marginPagesDisplayed={pageRange}
                    pageRangeDisplayed={pageRange}
                    containerClassName={'pagination'}
                    subContainerClassName={'pagination-inner'}
                    activeLinkClassName={'pagination-active'}
                  />
            </div>
          </>
          :
          <div className="spinner-container">
            <Spinner className="spinner-load"/>
          </div>}
      </div>     
    </div>
  )
}

export default AdminHome
