import React, { useState } from 'react';
import { Col } from 'reactstrap';
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faPen, faThumbsDown, faThumbsUp, faUserTimes } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';

type NewsGroupProps = {
    id: number,
    title: string,
    comments: string,
    hash: string,
    link: string,
    keywords: string[],
    createdDate: string,
    featured: boolean,
    approvalStatus: string,
    actionTitle: string,
    callback: (id: number, approvalStatus: string) => void
}

const NewsCard = (props: NewsGroupProps): React.ReactElement => {

    const [status, setStatus] = useState(props.approvalStatus)
    const [openModal, setOpenModal] = useState(false)

    const handlethumsup = (): void => {
        props.callback(props.id, 'Approved')
    }

    const handlethubsdown = (): void => {
        props.callback(props.id, 'Declined')
    }

    const openModel = (): void => {
        setOpenModal(!openModal)
    }

    return (
        <>
            <Col md={4} lg={3}>
                <div className="search-result-warp">
                    <figure>
                        <Image
                            height={80}
                            width={100}
                            layout="responsive"
                            loading="lazy"
                            src={`${(props.hash !== null) ?
                                process.env.NEXT_PUBLIC_API_HOST + 'api/file/view/' + props.hash
                                :
                                '/images/no-image.svg'
                                }`}
                            alt="image here"
                        />
                    </figure>
                    <a href={props.link} target="_blank">{props.title}</a>
                    <p className="font-weight-bold m-b-0 small text-secondary">
                        <span>
                            {props.keywords}
                        </span>
                    </p>
                    <p className={'m-b-25 small'}>{props.createdDate}</p>
                    <div className={'comment-box action-button'}>

                        {
                            status === 'Pending' ?
                                <div>
                                    <span className="approve"><FontAwesomeIcon icon={faThumbsUp} onClick={handlethumsup} /></span>
                                    <span className="decline"><FontAwesomeIcon icon={faThumbsDown} onClick={handlethubsdown} /></span>
                                    <Link href="/admin/update-news/[id]" as={`/admin/update-news/${props.id}`} scroll={true}>
                                        <span className="approve-with-note" ><FontAwesomeIcon icon={faPen} /></span>
                                    </Link>
                                </div>
                                : ''
                        }

                        {
                            status === 'Declined' ?
                                <div>
                                    <Link href="/admin/update-news/[id]" as={`/admin/update-news/${props.id}`} scroll={true}>
                                        <div>
                                            <span className="approve"><FontAwesomeIcon icon={faUserTimes} style={{ color: 'red' }} onClick={openModel} /></span>
                                            <span className="approve-with-note approve-edit" >Edit</span>
                                        </div>
                                    </Link>
                                </div>
                                : ''
                        }

                        {
                            status === 'Approved' ?
                                <div>

                                    <Link href="/admin/update-news/[id]" as={`/admin/update-news/${props.id}`} scroll={true}>
                                        <div>
                                            <span className="approve"><FontAwesomeIcon icon={faCheckCircle} onClick={openModel} /></span>
                                            <span className="approve-with-note approve-edit" >Edit</span>
                                        </div>
                                    </Link>
                                </div>
                                : ''
                        }

                        {
                            status === 'Approved with note' ?
                                <div>
                                    <span className="approve"><FontAwesomeIcon icon={faCheckCircle} style={{ color: 'blue' }} /></span>
                                    <Link href="/admin/update-news/[id]" as={`/admin/update-news/${props.id}`} scroll={true}>
                                        <span className="approve-with-note approve-edit">Edit</span>
                                    </Link>
                                </div>
                                : ''
                        }

                    </div>
                </div>

            </Col>
        </>
    )
}
export default NewsCard