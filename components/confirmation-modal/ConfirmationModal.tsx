import React from 'react';
import { Button, Col, Input, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';


type ConfirmationModalProps = {
    heading?: string,
    description?: string,
    deleteKey?: boolean,
    deleteFrom?: string,
    callback?: (confirmDelete?: boolean, deleteFrom?: string) => void
}


const ConfirmationModal = (props: ConfirmationModalProps): React.ReactElement<ConfirmationModalProps> => {

    const handleClick = () => {
        if (props.deleteKey) {
            props.callback(true, props.deleteFrom)
        }
        else {
            props.callback(false, props.deleteFrom)
        }
    }
    return (
        <div className="modal-mask">
            <div role="dialog" className="modal-container">
                <header id="modalTitle" className="modal-header">
                    <h3>{props.heading}</h3>
                    <Button type="button" aria-label="Close modal" className="btn-close"
                        onClick={() => props.callback(false, props.deleteFrom)}>X</Button>
                </header>
                <section id="modalDescription" className="modal-body">
                    <Row>
                        <Col md={12}>
                            <Row>
                                <Col sm={12}>
                                    {props.description}
                                </Col>
                            </Row>
                            <div className="confirm-btn">
                                <Input type="button" value="OK" color="success"
                                    onClick={handleClick}
                                />
                            </div>

                        </Col>
                    </Row>
                </section>
            </div>
        </div>
    )
}
export default ConfirmationModal
