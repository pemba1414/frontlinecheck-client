import Image from 'next/image'

const BannerList = ({ item }) => {
    return (
        <div className="ad-range">
            <Image
                height={10}
                width={100}
                layout="responsive"
                loading="lazy"
                src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${item}`}
                alt="image here"
            />
        </div>
    )
}

export default BannerList