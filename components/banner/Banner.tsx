import BannerList from './BannerList'
import { AdDetails } from '../../types/addetails.type'

const Banner = ({ banners }: { banners: AdDetails[] }) => {
    return (
        <div className="ad-select" >
            {banners.map((banner, index) => {
                return <BannerList key={index} item={banner.hash} />
            })}
        </div>
    )
}

export default Banner;