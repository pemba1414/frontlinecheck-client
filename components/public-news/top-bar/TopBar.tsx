import React from 'react';
import SocialShare from '../../social-share';

type TopBarProps = {
    hash?: string,
    userSlug?: string,
    callback: () => void
}

const TopBar = (props: TopBarProps): React.ReactElement<TopBarProps> => {
    return (
        <nav className="navbar navbar-expand-lg">
            
            <span className="container-fluid">
            <div className="main-logo">
                <a onClick={props.callback}>
                    <img src={`/images/logo.png`} width={120} className="img-fluid" alt="ad" />
                </a>
                </div>
                <div>
                    <span>
                        <SocialShare
                            url={`${process.env.NEXT_PUBLIC_API_HOST}${props.userSlug !== undefined ? props.userSlug : ''}`}
                            title={`Latest News ${props.userSlug !== undefined ? 'for ' + props.userSlug : ''}`}
                            size={27}
                            plainIcon={true} />
                    </span>
                    <span className="user-info">
                        <span>
                            <img src={`${props.hash ? `/api/file/view/${props.hash}` : '/images/avatar.png'}`}
                                width={120}
                                className="img-fluid"
                                alt="ad" />
                        </span>
                    </span>
                </div>
            </span>
           
        </nav >
    )
}
export default TopBar