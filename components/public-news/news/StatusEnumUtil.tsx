import {StatusEnum} from "./StatusEnum";

const convertStringToEnum = (status: string ) : string => {
    switch (status) {
        case StatusEnum.APPROVED:
            return "Approved"
        case StatusEnum.APPROVED_WITH_NOTE:
            return "Approved with note"
        case StatusEnum.DECLINED:
            return "Declined"
    }
}

export {
    convertStringToEnum
}