import React from 'react'
import { Row } from "reactstrap";
import NewsCard from "../news-card/NewsCard";
import { News } from "../news.type";

type NewsGroupProps = {
    newsList: News[],
    callback: (news: News) => void
}

const NewsGroup = (props: NewsGroupProps): React.ReactElement<News> => {
    return (
        <Row className="margin-reset">
            {
                props.newsList.map((news, index) => {
                    return <NewsCard key={`news-${index}`}
                        news={news}
                        callback={props.callback}
                    />
                })
            }
        </Row>
    )
}

export default NewsGroup
