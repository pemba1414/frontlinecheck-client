import React from 'react'
import { News } from "../news.type"
import SocialShare from '../../../social-share';
import { Col } from 'reactstrap';
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { useState } from 'react';
import ModalBox from '../../../popup-modal';
import { StatusEnum } from '../StatusEnum';
import { convertStringToEnum } from '../StatusEnumUtil';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image'

type NewsCardProps = {
    news: News,
    callback: (news: News) => void
}

const NewsCard = (props: NewsCardProps): React.ReactElement<News> => {
    const [openModal, setOpenModal] = useState(false);
    const [clickedTitle, setClickedTitle] = useState(true);

    const checkAprovalStatus = (approvalStatus: string): object => {
        let classTag = '',
            status = '';
        if (convertStringToEnum(approvalStatus) == StatusEnum.APPROVED) {
            classTag = 'tag h5';
            status = 'Valid';
        } else if (convertStringToEnum(approvalStatus) == StatusEnum.APPROVED_WITH_NOTE) {
            classTag = 'comm-tag h5';
            status = 'Commentary';
        } else {
            classTag = 'invalid-tag h5';
            status = 'Invalid';
        }
        return [classTag, status]
    }

    const isModalOpen = (approvalStatus: string): void => {
        if (approvalStatus == StatusEnum.APPROVED_WITH_NOTE) {
            setOpenModal(true);
        }
    }

    const getNewsDetail = () => {
        return (
            <div className="news-title"
                onClick={() => {
                    isModalOpen(props.news.approvalStatus)
                }}>
                <h3>{(props.news.actionTitle == undefined || props.news.actionTitle == '') ?
                    props.news.title.length > 60 ?
                        props.news.title.substring(0, 60) + '...'
                        :
                        props.news.title
                    :
                    props.news.actionTitle}</h3>

            </div>
        )
    }

    const handleOnClick = () => {
        props.callback(props.news)
    }

    const getInnerCard = (): React.ReactElement => {
        return (
            <div className="inner-card">
                <div className="image-wrap">
                    <Image
                        layout="fill"
                        loading="lazy"
                        quality="100"
                        src={`${(props.news.hash !== null) ?
                            process.env.NEXT_PUBLIC_API_HOST + 'api/file/view/' + props.news.hash
                            :
                            '/images/no-image.svg'
                            }
                        `}
                    />
                </div>
                <div className="source-web">
                    <Image
                        height={20}
                        width={20}
                        object-position="right bottom"
                        layout="responsive"
                        loading="lazy"
                        quality="100"
                        src={`${(props.news.websiteImageHash !== null) ?
                            process.env.NEXT_PUBLIC_API_HOST + 'api/file/view/' + props.news.websiteImageHash
                            :
                            '/images/no-image.svg'
                            }
                        `}
                    />
                </div>
                {(props.news.approvalStatus === StatusEnum.APPROVED_WITH_NOTE) ?
                    <div className="comm-note middle">
                        <a  onClick={() => {
                            setClickedTitle(false)
                            isModalOpen(props.news.approvalStatus)
                        }}>
                            <div className="text">
                                <h5>
                                    {
                                        ((props.news.comments.replace(/<[^>]+>/g, '')).replace(/&nbsp;/g, '').replace(/&yen;/, 'र्‍').replace(/&ndash;/, '–')).substring(0, 150)
                                    }
                                </h5>
                            </div>
                        </a>
                    </div>
                    :
                    ''
                }

                {(props.news.approvalStatus === StatusEnum.APPROVED) ?
                    <a href={props.news.link} target="_blank" rel="noopener noreferrer">
                        {getNewsDetail()}
                    </a>
                    :
                    getNewsDetail()
                }

                <div className="date-stamp">
                    <h6><FontAwesomeIcon icon={faCalendarAlt} />
                        {` ${props.news.createdDate}`}</h6>
                </div>
                <div className="d-flex card-bottom">
                    <div className={`${checkAprovalStatus(props.news.approvalStatus)[0]}`}
                        onClick={() => {
                            setClickedTitle(false)
                            isModalOpen(props.news.approvalStatus)
                        }}>
                        <h5>{checkAprovalStatus(props.news.approvalStatus)[1]}</h5>
                    </div>
                    <div className="bottom-socialmedia" onClick={handleOnClick}>
                        <SocialShare url={`${process.env.NEXT_PUBLIC_API_HOST}${props.news.userUrlSlug}?uri=${props.news.publicUrlSlug}`}
                            title={props.news.actionTitle !== null ? props.news.actionTitle : props.news.title}
                            shareText='शेयर'
                        />
                    </div>
                </div>
            </div>
        )
    }

    return (
        <Col md={3}>
            <div className="card">
                {(props.news.approvalStatus === StatusEnum.APPROVED_WITH_NOTE) ?
                    <div className="comm-card">
                        {getInnerCard()}
                    </div>
                    :
                    getInnerCard()
                }
            </div>
            <>
                {openModal ?
                    <ModalBox
                        actionTitle={props.news.actionTitle}
                        title={props.news.title}
                        link={props.news.link}
                        createdDate={props.news.createdDate}
                        approvalStatus={props.news.approvalStatus}
                        hash={props.news.hash}
                        comments={props.news.comments}
                        publicUrlSlug={props.news.publicUrlSlug}
                        userUrlSlug={props.news.userUrlSlug}
                        callback={() => setOpenModal(false)}
                    />
                    :
                    ""
                }
            </>
        </Col>
    )
}
export default NewsCard