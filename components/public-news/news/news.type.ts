export type News = {
    id: number,
    title: string,
    link?: string, //TODO(PS) make link required
    hash: string,
    approvalStatus: string,
    createdDate: string,
    actionTitle: string,
    comments: string,
    keywords?: string[],
    publicUrlSlug?: string,
    userUrlSlug: string,
    // websiteName: string,
    websiteImageHash?: string,
    // commentContainsImage: boolean,
    featured: boolean
}

export type NewsList = {
    news: News[]
}