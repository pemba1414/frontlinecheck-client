export enum StatusEnum {
    APPROVED = "Approved",
    APPROVED_WITH_NOTE = 'Approved with note',
    DECLINED = 'Declined'
}
