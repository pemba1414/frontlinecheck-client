import React from 'react'
import { Row } from "reactstrap";
import { Ads, AdsList } from "../ads.type"
import TileAd from './../tile-ad'

const AdGroup = (props: AdsList): React.ReactElement<Ads> => {
    return (
        <Row className="ad-v1">
            {props.ads.map((ads, index) => {
                return <TileAd key={`news-${index}`}
                    index={index} // not required if advertisement image list is available
                    hash={ads.hash}
                    link={ads.link}
                />
            })}
        </Row>
    )
}

export default AdGroup

