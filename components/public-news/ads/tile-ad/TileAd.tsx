import React from 'react'
import { Col } from "reactstrap";
import { Ads } from '../ads.type';
import Image from 'next/image'

const TileAd = (props: Ads): React.ReactElement<Ads> => {
    //alternative for advertisement image list
    const imgSource = ["300x250-frontline", "300x250-ad", "300x250-2-frontline-01", "300x250-frontline"]
    return (
        <Col md={3}>
            <a href={props.link} target="_blank">
                {(props.hash === "" || props.hash === undefined) ?
                    <Image
                        height={250}
                        width={300}
                        layout="responsive"
                        src={`/images/${imgSource[props.index]}.png`}
                        className="img-fluid"
                    />
                    :
                    <Image
                        height={250}
                        width={300}
                        layout="responsive"
                        src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${props.hash}`}
                        className="img-fluid"
                    />
                }
            </a>
        </Col>
    )
}

export default TileAd
