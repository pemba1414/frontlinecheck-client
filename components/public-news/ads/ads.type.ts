export type Ads = {
   index?: number,
   hash?: string,
   link?: string,
}

export type AdsList = {
   ads: Ads[]
}
