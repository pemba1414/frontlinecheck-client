import React from 'react';
import { Ads } from '../ads.type';
import Image from 'next/image';

const BannerAd = ({ ad }: { ad: Ads }): React.ReactElement<Ads> => {
    return (
        <div className="row margin-reset advertisement">
            <div className="col-lg-12">
                {ad !== undefined ?
                    <a href={ad.link}>
                        <Image
                            height={100}
                            width={1240}
                            layout="responsive"
                            loading="lazy"
                            src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${ad.hash}`}
                        />
                    </a>
                    :
                    <Image
                        height={100}
                        width={1240}
                        layout="responsive"
                        loading="lazy"
                        quality="100"
                        src={`/images/long-ad.png`}
                        alt="ad"
                    />
                }
            </div>
        </div>
    )
}
export default BannerAd