import React, { useState, useEffect } from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import TopBar from './top-bar'
import BannerAd from './ads/banner-ad'
import AdGroup from './ads/ad-group'
import { News } from './news/news.type'
import ModalBox from '../popup-modal/ModalBox'
import { Ads } from './ads/ads.type'
import NewsGroup from './news/news-group/NewsGroup'
import { Alert, Button, Modal, ModalBody, ModalHeader, Spinner } from 'reactstrap'
import { useRouter } from 'next/dist/client/router'
import Head from 'next/head'
import ConfirmationModal from '../confirmation-modal'

type PublicNewsProps = {
    userSlug?: string
}

const PublicNews = (props: PublicNewsProps): React.ReactElement<PublicNewsProps> => {
    const [openModal, setOpenModal] = useState(false)
    const [news, setNews] = useState<News[]>([])
    const [ads, setAds] = useState<Ads[]>([])
    const [bannerAds, setBannerAds] = useState<Ads[]>([])
    const [totalPages, setTotalPages] = useState(0)
    const [perPage, setPerPage] = useState(0)
    const [currentPage, setCurrentPage] = useState(0)
    const [loadingSpinner, setLoadingSpinner] = useState<boolean>(true)
    const [userCheck, setUserCheck] = useState<boolean>(true)
    const [newsCheck, setNewsCheck] = useState<boolean>(true)
    const [sideLogo, setSideLogo] = useState(null)
    const [selectedNews, setSelectedNews] = useState<News>()
    const [title, setTitle] = useState('Frontlinecheck')
    const [description, setDescription] = useState('')
    const [imagePath, setImagePath] = useState('/images/600x315-frontline-01.png')
    const { query } = useRouter();
    const [invalidNewsUriPopup, setInvalidNewsUriPopup] = useState(false);
    const toggle = () => {
        window.history.pushState(null, '', window.location.href.replace(/\?.*$/g, ''))
        setSelectedNews(null);
        setInvalidNewsUriPopup(false);
    }

    useEffect(() => {
        if (query?.uri !== undefined) {
            axios.get(`/api/public/v2/news/${query.uri}`)
                .then((response) => {
                    setSelectedNews(response.data)
                    setOpenModal(true)
                })
                .catch((error) => {
                    console.error(error)
                    setNewsCheck(false)
                    setInvalidNewsUriPopup(true)
                    setTimeout(toggle, 4000)
                })
        }
    }, [])

    useEffect(() => {
        axios.get(`/api/public/v2/news/${props.userSlug != undefined ? 'user/' + props.userSlug : 'featured'}?page=${currentPage}`)
            .then((response) => {
                setNews(response.data.results.list)
                setAds(response.data.tileAds)
                setBannerAds(response.data.bannerAds)
                setTotalPages(response.data.results.totalPages)
                setPerPage(response.data.results.size)
                setLoadingSpinner(false)
                setSideLogo(response.data.profilePictureHash)
            })
            .catch((error) => {
                console.error(error)
                setOpenModal(false)
                setUserCheck(false)
            })
    }, [currentPage])

    const onChangePage = (data) => {
        setLoadingSpinner(true)
        setCurrentPage(data.selected)
    }

    const redirectToHome = (): void => {
        if (currentPage == 0) {
            return
        }
        setLoadingSpinner(true)
        setCurrentPage(0)
    }

    const defaultAds = [{
        "id": 111,
        "name": "advertisement 1",
        "link": `${process.env.NEXT_PUBLIC_API_HOST}${props.userSlug}`,
        "hash": undefined,
        "type": "TILE"
    }, {
        "id": 112,
        "name": "advertisement 2",
        "link": `${process.env.NEXT_PUBLIC_API_HOST}${props.userSlug}`,
        "hash": undefined,
        "type": "TILE"
    }, {
        "id": 113,
        "name": "advertisement 3",
        "link": `${process.env.NEXT_PUBLIC_API_HOST}${props.userSlug}`,
        "hash": undefined,
        "type": "TILE"
    }, {
        "id": 114,
        "name": "advertisement 4",
        "link": `${process.env.NEXT_PUBLIC_API_HOST}${props.userSlug}`,
        "hash": undefined,
        "type": "TILE"
    }]

    const handleCallback = () => {
        window.history.pushState(null, '', window.location.href.replace(/\?.*$/g, ''))
        setSelectedNews(null)
    }

    const handleNewsShare = (news: News) => {
        setTitle(news.title)
        if (news.comments !== null) {
            setDescription(news.comments)
        }
        if (news.hash !== null) {
            setImagePath(`/api/file/view/${news.hash}`)
        }
    }

    return (
        <>
            <div id="wrapper">
                <TopBar hash={sideLogo} userSlug={props.userSlug} callback={redirectToHome} />
                <div className="container-fluid content-warp">
                    <BannerAd ad={bannerAds[0]} />
                    {userCheck ?
                        !loadingSpinner ?
                            news.length > 0 ?
                                <>
                                    <NewsGroup newsList={news.slice(0, 4)} callback={handleNewsShare} />
                                    <AdGroup ads={ads.slice(0, 4)} />
                                    {(news.slice(4, 8).length > 0) ?
                                        <>
                                            <NewsGroup newsList={news.slice(4, 8)} callback={handleNewsShare} />
                                            <AdGroup ads={ads.slice(4, 8)} />
                                            <NewsGroup newsList={news.slice(8, 12)} callback={handleNewsShare} />
                                        </>
                                        :
                                        ''
                                    }
                                    <div className={`container ${(userCheck) ? '' : 'hide'}`}>
                                        <ReactPaginate
                                            previousLabel={'PREV'}
                                            nextLabel={'NEXT'}
                                            breakLabel={'...'}
                                            pageCount={totalPages}
                                            forcePage={currentPage}
                                            onPageChange={onChangePage}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={2}
                                            containerClassName={'pagination'}
                                            subContainerClassName={'pagination-inner'}
                                            activeLinkClassName={'pagination-active'}
                                        />
                                    </div>
                                    {!newsCheck ?
                                        <div>
                                            <Modal isOpen={invalidNewsUriPopup} toggle={toggle}>
                                                <ModalHeader>NEWS NOT FOUND!</ModalHeader>
                                                <ModalBody>
                                                    The news you are looking for does not exist in
                                                    user: {props.userSlug}
                                                    <p>Please check the News uri and try again.</p>
                                                </ModalBody>
                                            </Modal>
                                        </div>
                                        :
                                        <></>
                                    }
                                </>
                                :
                                <div className="news-null">
                                    <Alert color="danger">
                                        <h1 className="alert-heading">NEWS NOT FOUND!</h1>
                                        <hr />
                                        <h5 className="mb-0">
                                            {props.userSlug ? 'No news for "' + props.userSlug : ''}
                                        </h5>
                                    </Alert>
                                    <AdGroup ads={defaultAds} />

                                </div>
                            :
                            <>
                                <div className="loader">
                                    <Spinner className="spinner" style={{ width: '5rem', height: '5rem' }}
                                        color="#2c3e50" />
                                </div>
                            </>
                        :
                        <div className="news-null">
                            <Alert color="danger">
                                <h1 className="alert-heading">USER NOT FOUND!</h1>
                                <hr />
                                <h5 className="mb-0">
                                    {props.userSlug ? 'User "' + props.userSlug + '" does not exist.' : ''}
                                    <p>Please check the url or username and try again</p>
                                </h5>
                            </Alert>
                            <AdGroup ads={defaultAds} />
                        </div>
                    }
                    <BannerAd ad={bannerAds[1]} />
                </div>
            </div>

            <>
                {
                    userCheck ?
                        selectedNews && openModal ?
                            <ModalBox
                                title={selectedNews.title}
                                actionTitle={selectedNews.actionTitle}
                                createdDate={selectedNews.createdDate}
                                approvalStatus={selectedNews.approvalStatus}
                                hash={selectedNews.hash}
                                link={selectedNews.link}
                                publicUrlSlug={selectedNews.publicUrlSlug}
                                userUrlSlug={selectedNews.userUrlSlug}
                                comments={selectedNews.comments}
                                callback={handleCallback}
                            />
                            :
                            ''
                        :
                        ''
                }
            </>
        </>
    )
}

export default PublicNews
