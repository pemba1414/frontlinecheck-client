import Image from 'next/image'
const TileList = ({ item }) => {
    return (
        <div className="title-ads">
            <Image
                height={40}
                width={40}
                layout="responsive"
                loading="lazy"
                src={`${process.env.NEXT_PUBLIC_API_HOST}api/file/view/${item}`}
                alt="image here"
            />
        </div>
    )
}

export default TileList