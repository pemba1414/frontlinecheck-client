import TileList from './TileList'
import { AdDetails } from '../../types/addetails.type'

const Tile = ({ tiles }: { tiles: AdDetails[] }) => {
    return (
        <div>
            {tiles.map((tile, index) => {
                return <TileList key="index" item={tile.hash} />
            })}
        </div>
    )
}

export default Tile;